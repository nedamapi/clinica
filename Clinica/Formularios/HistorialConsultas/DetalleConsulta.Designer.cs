﻿namespace Clinica.Formularios.HistorialConsultas
{
    partial class DetalleConsulta
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PanelTitulo = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.BtnMinimizar = new System.Windows.Forms.Button();
            this.BtnMaximizar = new System.Windows.Forms.Button();
            this.BtnCerrar = new System.Windows.Forms.Button();
            this.TxtRLADD = new System.Windows.Forms.TextBox();
            this.TxtRLDIP = new System.Windows.Forms.TextBox();
            this.TxtRLOI = new System.Windows.Forms.TextBox();
            this.TxtRLOD = new System.Windows.Forms.TextBox();
            this.TxtARDIP = new System.Windows.Forms.TextBox();
            this.TxtAROI = new System.Windows.Forms.TextBox();
            this.TxtAROD = new System.Windows.Forms.TextBox();
            this.TxtTratamiento = new System.Windows.Forms.TextBox();
            this.TxtPlan = new System.Windows.Forms.TextBox();
            this.TxtDiagnostico = new System.Windows.Forms.TextBox();
            this.TxtBiomicrospia = new System.Windows.Forms.TextBox();
            this.TxtFondoscopia = new System.Windows.Forms.TextBox();
            this.TxtPIOI = new System.Windows.Forms.TextBox();
            this.TxtPIOD = new System.Windows.Forms.TextBox();
            this.TxtSSJ = new System.Windows.Forms.TextBox();
            this.TxtSSOI = new System.Windows.Forms.TextBox();
            this.TxtSSOD = new System.Windows.Forms.TextBox();
            this.TxtSCJ = new System.Windows.Forms.TextBox();
            this.TxtSCOI = new System.Windows.Forms.TextBox();
            this.TxtSCOD = new System.Windows.Forms.TextBox();
            this.TxtPupilas = new System.Windows.Forms.TextBox();
            this.TxtIris = new System.Windows.Forms.TextBox();
            this.TxtCorneas = new System.Windows.Forms.TextBox();
            this.TxtConjuntivas = new System.Windows.Forms.TextBox();
            this.TxtParpados = new System.Windows.Forms.TextBox();
            this.TxtInspeccionFisica = new System.Windows.Forms.TextBox();
            this.TxtLAADD = new System.Windows.Forms.TextBox();
            this.TxtLADIP = new System.Windows.Forms.TextBox();
            this.TxtLAOI = new System.Windows.Forms.TextBox();
            this.TxtLAOD = new System.Windows.Forms.TextBox();
            this.TxtAntecedentes = new System.Windows.Forms.TextBox();
            this.TxtUltimaConsulta = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.LblLentes = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.LblMotivoConsulta = new System.Windows.Forms.Label();
            this.dtpFechaConsulta = new System.Windows.Forms.DateTimePicker();
            this.PanelTitulo.SuspendLayout();
            this.SuspendLayout();
            // 
            // PanelTitulo
            // 
            this.PanelTitulo.BackColor = System.Drawing.SystemColors.Highlight;
            this.PanelTitulo.Controls.Add(this.label1);
            this.PanelTitulo.Controls.Add(this.BtnMinimizar);
            this.PanelTitulo.Controls.Add(this.BtnMaximizar);
            this.PanelTitulo.Controls.Add(this.BtnCerrar);
            this.PanelTitulo.Dock = System.Windows.Forms.DockStyle.Top;
            this.PanelTitulo.Location = new System.Drawing.Point(0, 0);
            this.PanelTitulo.Name = "PanelTitulo";
            this.PanelTitulo.Size = new System.Drawing.Size(1041, 28);
            this.PanelTitulo.TabIndex = 120;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.Control;
            this.label1.Location = new System.Drawing.Point(47, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(171, 21);
            this.label1.TabIndex = 3;
            this.label1.Text = "Detalle de Consultas";
            // 
            // BtnMinimizar
            // 
            this.BtnMinimizar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnMinimizar.FlatAppearance.BorderSize = 0;
            this.BtnMinimizar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnMinimizar.Image = global::Clinica.Properties.Resources.icons8_minimizar_la_ventana_48;
            this.BtnMinimizar.Location = new System.Drawing.Point(932, 0);
            this.BtnMinimizar.Name = "BtnMinimizar";
            this.BtnMinimizar.Size = new System.Drawing.Size(37, 28);
            this.BtnMinimizar.TabIndex = 2;
            this.BtnMinimizar.UseVisualStyleBackColor = true;
            this.BtnMinimizar.Click += new System.EventHandler(this.BtnMinimizar_Click);
            // 
            // BtnMaximizar
            // 
            this.BtnMaximizar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnMaximizar.Enabled = false;
            this.BtnMaximizar.FlatAppearance.BorderSize = 0;
            this.BtnMaximizar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnMaximizar.Image = global::Clinica.Properties.Resources.icons8_maximizar_la_ventana_48;
            this.BtnMaximizar.Location = new System.Drawing.Point(966, 0);
            this.BtnMaximizar.Name = "BtnMaximizar";
            this.BtnMaximizar.Size = new System.Drawing.Size(40, 28);
            this.BtnMaximizar.TabIndex = 1;
            this.BtnMaximizar.UseVisualStyleBackColor = true;
            // 
            // BtnCerrar
            // 
            this.BtnCerrar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnCerrar.FlatAppearance.BorderSize = 0;
            this.BtnCerrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnCerrar.Image = global::Clinica.Properties.Resources.icons8_cerrar_ventana_48;
            this.BtnCerrar.Location = new System.Drawing.Point(1004, 0);
            this.BtnCerrar.Name = "BtnCerrar";
            this.BtnCerrar.Size = new System.Drawing.Size(37, 28);
            this.BtnCerrar.TabIndex = 0;
            this.BtnCerrar.UseVisualStyleBackColor = true;
            this.BtnCerrar.Click += new System.EventHandler(this.BtnCerrar_Click);
            // 
            // TxtRLADD
            // 
            this.TxtRLADD.Location = new System.Drawing.Point(812, 719);
            this.TxtRLADD.Name = "TxtRLADD";
            this.TxtRLADD.ReadOnly = true;
            this.TxtRLADD.Size = new System.Drawing.Size(130, 20);
            this.TxtRLADD.TabIndex = 290;
            // 
            // TxtRLDIP
            // 
            this.TxtRLDIP.Location = new System.Drawing.Point(812, 693);
            this.TxtRLDIP.Name = "TxtRLDIP";
            this.TxtRLDIP.ReadOnly = true;
            this.TxtRLDIP.Size = new System.Drawing.Size(130, 20);
            this.TxtRLDIP.TabIndex = 289;
            // 
            // TxtRLOI
            // 
            this.TxtRLOI.Location = new System.Drawing.Point(812, 667);
            this.TxtRLOI.Name = "TxtRLOI";
            this.TxtRLOI.ReadOnly = true;
            this.TxtRLOI.Size = new System.Drawing.Size(130, 20);
            this.TxtRLOI.TabIndex = 288;
            // 
            // TxtRLOD
            // 
            this.TxtRLOD.Location = new System.Drawing.Point(812, 641);
            this.TxtRLOD.Name = "TxtRLOD";
            this.TxtRLOD.ReadOnly = true;
            this.TxtRLOD.Size = new System.Drawing.Size(130, 20);
            this.TxtRLOD.TabIndex = 287;
            // 
            // TxtARDIP
            // 
            this.TxtARDIP.Location = new System.Drawing.Point(88, 698);
            this.TxtARDIP.Name = "TxtARDIP";
            this.TxtARDIP.ReadOnly = true;
            this.TxtARDIP.Size = new System.Drawing.Size(130, 20);
            this.TxtARDIP.TabIndex = 286;
            // 
            // TxtAROI
            // 
            this.TxtAROI.Location = new System.Drawing.Point(88, 672);
            this.TxtAROI.Name = "TxtAROI";
            this.TxtAROI.ReadOnly = true;
            this.TxtAROI.Size = new System.Drawing.Size(130, 20);
            this.TxtAROI.TabIndex = 285;
            // 
            // TxtAROD
            // 
            this.TxtAROD.Location = new System.Drawing.Point(88, 646);
            this.TxtAROD.Name = "TxtAROD";
            this.TxtAROD.ReadOnly = true;
            this.TxtAROD.Size = new System.Drawing.Size(130, 20);
            this.TxtAROD.TabIndex = 284;
            // 
            // TxtTratamiento
            // 
            this.TxtTratamiento.Location = new System.Drawing.Point(218, 567);
            this.TxtTratamiento.Name = "TxtTratamiento";
            this.TxtTratamiento.ReadOnly = true;
            this.TxtTratamiento.Size = new System.Drawing.Size(756, 20);
            this.TxtTratamiento.TabIndex = 283;
            // 
            // TxtPlan
            // 
            this.TxtPlan.Location = new System.Drawing.Point(534, 516);
            this.TxtPlan.Multiline = true;
            this.TxtPlan.Name = "TxtPlan";
            this.TxtPlan.ReadOnly = true;
            this.TxtPlan.Size = new System.Drawing.Size(440, 41);
            this.TxtPlan.TabIndex = 282;
            // 
            // TxtDiagnostico
            // 
            this.TxtDiagnostico.Location = new System.Drawing.Point(55, 516);
            this.TxtDiagnostico.Multiline = true;
            this.TxtDiagnostico.Name = "TxtDiagnostico";
            this.TxtDiagnostico.ReadOnly = true;
            this.TxtDiagnostico.Size = new System.Drawing.Size(451, 41);
            this.TxtDiagnostico.TabIndex = 281;
            // 
            // TxtBiomicrospia
            // 
            this.TxtBiomicrospia.Location = new System.Drawing.Point(534, 456);
            this.TxtBiomicrospia.Multiline = true;
            this.TxtBiomicrospia.Name = "TxtBiomicrospia";
            this.TxtBiomicrospia.ReadOnly = true;
            this.TxtBiomicrospia.Size = new System.Drawing.Size(440, 41);
            this.TxtBiomicrospia.TabIndex = 280;
            // 
            // TxtFondoscopia
            // 
            this.TxtFondoscopia.Location = new System.Drawing.Point(55, 456);
            this.TxtFondoscopia.Multiline = true;
            this.TxtFondoscopia.Name = "TxtFondoscopia";
            this.TxtFondoscopia.ReadOnly = true;
            this.TxtFondoscopia.Size = new System.Drawing.Size(451, 41);
            this.TxtFondoscopia.TabIndex = 279;
            // 
            // TxtPIOI
            // 
            this.TxtPIOI.Location = new System.Drawing.Point(711, 414);
            this.TxtPIOI.Name = "TxtPIOI";
            this.TxtPIOI.ReadOnly = true;
            this.TxtPIOI.Size = new System.Drawing.Size(130, 20);
            this.TxtPIOI.TabIndex = 278;
            // 
            // TxtPIOD
            // 
            this.TxtPIOD.Location = new System.Drawing.Point(711, 388);
            this.TxtPIOD.Name = "TxtPIOD";
            this.TxtPIOD.ReadOnly = true;
            this.TxtPIOD.Size = new System.Drawing.Size(130, 20);
            this.TxtPIOD.TabIndex = 277;
            // 
            // TxtSSJ
            // 
            this.TxtSSJ.Location = new System.Drawing.Point(475, 392);
            this.TxtSSJ.Name = "TxtSSJ";
            this.TxtSSJ.ReadOnly = true;
            this.TxtSSJ.Size = new System.Drawing.Size(65, 20);
            this.TxtSSJ.TabIndex = 276;
            // 
            // TxtSSOI
            // 
            this.TxtSSOI.Location = new System.Drawing.Point(381, 392);
            this.TxtSSOI.Name = "TxtSSOI";
            this.TxtSSOI.ReadOnly = true;
            this.TxtSSOI.Size = new System.Drawing.Size(65, 20);
            this.TxtSSOI.TabIndex = 275;
            // 
            // TxtSSOD
            // 
            this.TxtSSOD.Location = new System.Drawing.Point(283, 392);
            this.TxtSSOD.Name = "TxtSSOD";
            this.TxtSSOD.ReadOnly = true;
            this.TxtSSOD.Size = new System.Drawing.Size(65, 20);
            this.TxtSSOD.TabIndex = 274;
            // 
            // TxtSCJ
            // 
            this.TxtSCJ.Location = new System.Drawing.Point(475, 361);
            this.TxtSCJ.Name = "TxtSCJ";
            this.TxtSCJ.ReadOnly = true;
            this.TxtSCJ.Size = new System.Drawing.Size(65, 20);
            this.TxtSCJ.TabIndex = 273;
            // 
            // TxtSCOI
            // 
            this.TxtSCOI.Location = new System.Drawing.Point(381, 361);
            this.TxtSCOI.Name = "TxtSCOI";
            this.TxtSCOI.ReadOnly = true;
            this.TxtSCOI.Size = new System.Drawing.Size(65, 20);
            this.TxtSCOI.TabIndex = 272;
            // 
            // TxtSCOD
            // 
            this.TxtSCOD.Location = new System.Drawing.Point(283, 361);
            this.TxtSCOD.Name = "TxtSCOD";
            this.TxtSCOD.ReadOnly = true;
            this.TxtSCOD.Size = new System.Drawing.Size(65, 20);
            this.TxtSCOD.TabIndex = 271;
            // 
            // TxtPupilas
            // 
            this.TxtPupilas.Location = new System.Drawing.Point(218, 335);
            this.TxtPupilas.Name = "TxtPupilas";
            this.TxtPupilas.ReadOnly = true;
            this.TxtPupilas.Size = new System.Drawing.Size(756, 20);
            this.TxtPupilas.TabIndex = 270;
            // 
            // TxtIris
            // 
            this.TxtIris.Location = new System.Drawing.Point(218, 309);
            this.TxtIris.Name = "TxtIris";
            this.TxtIris.ReadOnly = true;
            this.TxtIris.Size = new System.Drawing.Size(756, 20);
            this.TxtIris.TabIndex = 269;
            // 
            // TxtCorneas
            // 
            this.TxtCorneas.Location = new System.Drawing.Point(218, 283);
            this.TxtCorneas.Name = "TxtCorneas";
            this.TxtCorneas.ReadOnly = true;
            this.TxtCorneas.Size = new System.Drawing.Size(756, 20);
            this.TxtCorneas.TabIndex = 268;
            // 
            // TxtConjuntivas
            // 
            this.TxtConjuntivas.Location = new System.Drawing.Point(218, 257);
            this.TxtConjuntivas.Name = "TxtConjuntivas";
            this.TxtConjuntivas.ReadOnly = true;
            this.TxtConjuntivas.Size = new System.Drawing.Size(756, 20);
            this.TxtConjuntivas.TabIndex = 267;
            // 
            // TxtParpados
            // 
            this.TxtParpados.Location = new System.Drawing.Point(218, 231);
            this.TxtParpados.Name = "TxtParpados";
            this.TxtParpados.ReadOnly = true;
            this.TxtParpados.Size = new System.Drawing.Size(756, 20);
            this.TxtParpados.TabIndex = 266;
            // 
            // TxtInspeccionFisica
            // 
            this.TxtInspeccionFisica.Location = new System.Drawing.Point(218, 205);
            this.TxtInspeccionFisica.Name = "TxtInspeccionFisica";
            this.TxtInspeccionFisica.ReadOnly = true;
            this.TxtInspeccionFisica.Size = new System.Drawing.Size(756, 20);
            this.TxtInspeccionFisica.TabIndex = 265;
            // 
            // TxtLAADD
            // 
            this.TxtLAADD.Location = new System.Drawing.Point(824, 179);
            this.TxtLAADD.Name = "TxtLAADD";
            this.TxtLAADD.ReadOnly = true;
            this.TxtLAADD.Size = new System.Drawing.Size(130, 20);
            this.TxtLAADD.TabIndex = 264;
            // 
            // TxtLADIP
            // 
            this.TxtLADIP.Location = new System.Drawing.Point(626, 179);
            this.TxtLADIP.Name = "TxtLADIP";
            this.TxtLADIP.ReadOnly = true;
            this.TxtLADIP.Size = new System.Drawing.Size(130, 20);
            this.TxtLADIP.TabIndex = 263;
            // 
            // TxtLAOI
            // 
            this.TxtLAOI.Location = new System.Drawing.Point(435, 179);
            this.TxtLAOI.Name = "TxtLAOI";
            this.TxtLAOI.ReadOnly = true;
            this.TxtLAOI.Size = new System.Drawing.Size(130, 20);
            this.TxtLAOI.TabIndex = 262;
            // 
            // TxtLAOD
            // 
            this.TxtLAOD.Location = new System.Drawing.Point(245, 179);
            this.TxtLAOD.Name = "TxtLAOD";
            this.TxtLAOD.ReadOnly = true;
            this.TxtLAOD.Size = new System.Drawing.Size(130, 20);
            this.TxtLAOD.TabIndex = 261;
            // 
            // TxtAntecedentes
            // 
            this.TxtAntecedentes.Location = new System.Drawing.Point(218, 152);
            this.TxtAntecedentes.Name = "TxtAntecedentes";
            this.TxtAntecedentes.ReadOnly = true;
            this.TxtAntecedentes.Size = new System.Drawing.Size(756, 20);
            this.TxtAntecedentes.TabIndex = 260;
            // 
            // TxtUltimaConsulta
            // 
            this.TxtUltimaConsulta.Location = new System.Drawing.Point(50, 98);
            this.TxtUltimaConsulta.Multiline = true;
            this.TxtUltimaConsulta.Name = "TxtUltimaConsulta";
            this.TxtUltimaConsulta.ReadOnly = true;
            this.TxtUltimaConsulta.Size = new System.Drawing.Size(924, 41);
            this.TxtUltimaConsulta.TabIndex = 259;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(785, 182);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(33, 13);
            this.label13.TabIndex = 258;
            this.label13.Text = "ADD:";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(592, 182);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(28, 13);
            this.label21.TabIndex = 257;
            this.label21.Text = "DIP:";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(667, 364);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(101, 13);
            this.label43.TabIndex = 256;
            this.label43.Text = "Presión Intraocular: ";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(454, 395);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(15, 13);
            this.label40.TabIndex = 255;
            this.label40.Text = "J:";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(354, 395);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(21, 13);
            this.label41.TabIndex = 254;
            this.label41.Text = "OI:";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(251, 395);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(26, 13);
            this.label42.TabIndex = 253;
            this.label42.Text = "OD:";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(454, 364);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(15, 13);
            this.label39.TabIndex = 252;
            this.label39.Text = "J:";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(354, 364);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(21, 13);
            this.label37.TabIndex = 251;
            this.label37.Text = "OI:";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(251, 364);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(26, 13);
            this.label38.TabIndex = 250;
            this.label38.Text = "OD:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(669, 417);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(21, 13);
            this.label11.TabIndex = 235;
            this.label11.Text = "OI:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(669, 391);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(26, 13);
            this.label12.TabIndex = 234;
            this.label12.Text = "OD:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(221, 395);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(24, 13);
            this.label18.TabIndex = 233;
            this.label18.Text = "SS:";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(221, 364);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(24, 13);
            this.label19.TabIndex = 232;
            this.label19.Text = "SC:";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(781, 722);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(33, 13);
            this.label36.TabIndex = 249;
            this.label36.Text = "ADD:";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(781, 696);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(28, 13);
            this.label32.TabIndex = 248;
            this.label32.Text = "DIP:";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(781, 670);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(21, 13);
            this.label33.TabIndex = 247;
            this.label33.Text = "OI:";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(780, 644);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(26, 13);
            this.label34.TabIndex = 246;
            this.label34.Text = "OD:";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(775, 620);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(77, 13);
            this.label35.TabIndex = 245;
            this.label35.Text = "Receta Lentes";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(57, 701);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(28, 13);
            this.label31.TabIndex = 244;
            this.label31.Text = "DIP:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(57, 675);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(21, 13);
            this.label4.TabIndex = 243;
            this.label4.Text = "OI:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(56, 649);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(26, 13);
            this.label6.TabIndex = 242;
            this.label6.Text = "OD:";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(51, 625);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(65, 13);
            this.label29.TabIndex = 241;
            this.label29.Text = "Autorefracto";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(55, 570);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(66, 13);
            this.label28.TabIndex = 240;
            this.label28.Text = "Tratamiento:";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(531, 500);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(28, 13);
            this.label26.TabIndex = 239;
            this.label26.Text = "Plan";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(52, 500);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(63, 13);
            this.label27.TabIndex = 238;
            this.label27.Text = "Diagnostico";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(531, 443);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(66, 13);
            this.label25.TabIndex = 237;
            this.label25.Text = "Biomicrospia";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(52, 443);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(68, 13);
            this.label24.TabIndex = 236;
            this.label24.Text = "Fondoscopia";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(50, 364);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(41, 13);
            this.label20.TabIndex = 231;
            this.label20.Text = "Vision: ";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(50, 260);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(65, 13);
            this.label17.TabIndex = 230;
            this.label17.Text = "Conjuntivas:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(50, 286);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(49, 13);
            this.label16.TabIndex = 229;
            this.label16.Text = "Corneas:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(50, 312);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(23, 13);
            this.label15.TabIndex = 228;
            this.label15.Text = "Iris:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(50, 338);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(44, 13);
            this.label14.TabIndex = 227;
            this.label14.Text = "Pupilas:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(50, 234);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(55, 13);
            this.label10.TabIndex = 226;
            this.label10.Text = "Parpados:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(50, 208);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(91, 13);
            this.label9.TabIndex = 225;
            this.label9.Text = "Inspección física:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(408, 182);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(21, 13);
            this.label8.TabIndex = 224;
            this.label8.Text = "OI:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(215, 182);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(26, 13);
            this.label7.TabIndex = 223;
            this.label7.Text = "OD:";
            // 
            // LblLentes
            // 
            this.LblLentes.AutoSize = true;
            this.LblLentes.Location = new System.Drawing.Point(50, 182);
            this.LblLentes.Name = "LblLentes";
            this.LblLentes.Size = new System.Drawing.Size(94, 13);
            this.LblLentes.TabIndex = 222;
            this.LblLentes.Text = "Lentes anteriores: ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(50, 155);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(76, 13);
            this.label5.TabIndex = 221;
            this.label5.Text = "Antecedentes:";
            // 
            // LblMotivoConsulta
            // 
            this.LblMotivoConsulta.AutoSize = true;
            this.LblMotivoConsulta.Location = new System.Drawing.Point(47, 82);
            this.LblMotivoConsulta.Name = "LblMotivoConsulta";
            this.LblMotivoConsulta.Size = new System.Drawing.Size(110, 13);
            this.LblMotivoConsulta.TabIndex = 220;
            this.LblMotivoConsulta.Text = "Motivo útima consulta";
            // 
            // dtpFechaConsulta
            // 
            this.dtpFechaConsulta.Enabled = false;
            this.dtpFechaConsulta.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpFechaConsulta.Location = new System.Drawing.Point(862, 50);
            this.dtpFechaConsulta.Name = "dtpFechaConsulta";
            this.dtpFechaConsulta.Size = new System.Drawing.Size(112, 20);
            this.dtpFechaConsulta.TabIndex = 291;
            // 
            // DetalleConsulta
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1041, 751);
            this.Controls.Add(this.dtpFechaConsulta);
            this.Controls.Add(this.TxtRLADD);
            this.Controls.Add(this.TxtRLDIP);
            this.Controls.Add(this.TxtRLOI);
            this.Controls.Add(this.TxtRLOD);
            this.Controls.Add(this.TxtARDIP);
            this.Controls.Add(this.TxtAROI);
            this.Controls.Add(this.TxtAROD);
            this.Controls.Add(this.TxtTratamiento);
            this.Controls.Add(this.TxtPlan);
            this.Controls.Add(this.TxtDiagnostico);
            this.Controls.Add(this.TxtBiomicrospia);
            this.Controls.Add(this.TxtFondoscopia);
            this.Controls.Add(this.TxtPIOI);
            this.Controls.Add(this.TxtPIOD);
            this.Controls.Add(this.TxtSSJ);
            this.Controls.Add(this.TxtSSOI);
            this.Controls.Add(this.TxtSSOD);
            this.Controls.Add(this.TxtSCJ);
            this.Controls.Add(this.TxtSCOI);
            this.Controls.Add(this.TxtSCOD);
            this.Controls.Add(this.TxtPupilas);
            this.Controls.Add(this.TxtIris);
            this.Controls.Add(this.TxtCorneas);
            this.Controls.Add(this.TxtConjuntivas);
            this.Controls.Add(this.TxtParpados);
            this.Controls.Add(this.TxtInspeccionFisica);
            this.Controls.Add(this.TxtLAADD);
            this.Controls.Add(this.TxtLADIP);
            this.Controls.Add(this.TxtLAOI);
            this.Controls.Add(this.TxtLAOD);
            this.Controls.Add(this.TxtAntecedentes);
            this.Controls.Add(this.TxtUltimaConsulta);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label43);
            this.Controls.Add(this.label40);
            this.Controls.Add(this.label41);
            this.Controls.Add(this.label42);
            this.Controls.Add(this.label39);
            this.Controls.Add(this.label37);
            this.Controls.Add(this.label38);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label36);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.label34);
            this.Controls.Add(this.label35);
            this.Controls.Add(this.label31);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.LblLentes);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.LblMotivoConsulta);
            this.Controls.Add(this.PanelTitulo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "DetalleConsulta";
            this.Text = "DetalleConsulta";
            this.Load += new System.EventHandler(this.DetalleConsulta_Load);
            this.PanelTitulo.ResumeLayout(false);
            this.PanelTitulo.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel PanelTitulo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button BtnMinimizar;
        private System.Windows.Forms.Button BtnMaximizar;
        private System.Windows.Forms.Button BtnCerrar;
        private System.Windows.Forms.TextBox TxtRLADD;
        private System.Windows.Forms.TextBox TxtRLDIP;
        private System.Windows.Forms.TextBox TxtRLOI;
        private System.Windows.Forms.TextBox TxtRLOD;
        private System.Windows.Forms.TextBox TxtARDIP;
        private System.Windows.Forms.TextBox TxtAROI;
        private System.Windows.Forms.TextBox TxtAROD;
        private System.Windows.Forms.TextBox TxtTratamiento;
        private System.Windows.Forms.TextBox TxtPlan;
        private System.Windows.Forms.TextBox TxtDiagnostico;
        private System.Windows.Forms.TextBox TxtBiomicrospia;
        private System.Windows.Forms.TextBox TxtFondoscopia;
        private System.Windows.Forms.TextBox TxtPIOI;
        private System.Windows.Forms.TextBox TxtPIOD;
        private System.Windows.Forms.TextBox TxtSSJ;
        private System.Windows.Forms.TextBox TxtSSOI;
        private System.Windows.Forms.TextBox TxtSSOD;
        private System.Windows.Forms.TextBox TxtSCJ;
        private System.Windows.Forms.TextBox TxtSCOI;
        private System.Windows.Forms.TextBox TxtSCOD;
        private System.Windows.Forms.TextBox TxtPupilas;
        private System.Windows.Forms.TextBox TxtIris;
        private System.Windows.Forms.TextBox TxtCorneas;
        private System.Windows.Forms.TextBox TxtConjuntivas;
        private System.Windows.Forms.TextBox TxtParpados;
        private System.Windows.Forms.TextBox TxtInspeccionFisica;
        private System.Windows.Forms.TextBox TxtLAADD;
        private System.Windows.Forms.TextBox TxtLADIP;
        private System.Windows.Forms.TextBox TxtLAOI;
        private System.Windows.Forms.TextBox TxtLAOD;
        private System.Windows.Forms.TextBox TxtAntecedentes;
        private System.Windows.Forms.TextBox TxtUltimaConsulta;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label LblLentes;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label LblMotivoConsulta;
        private System.Windows.Forms.DateTimePicker dtpFechaConsulta;
    }
}