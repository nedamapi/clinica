﻿using Clinica.Clases;
using Clinica.Datos.BBDD;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Clinica.Formularios.HistorialConsultas
{
    public partial class HistorialConsulta : Form
    {
        public HistorialConsulta()
        {
            InitializeComponent();
        }

        OftalmologiaEntities DB = new OftalmologiaEntities();

        private void HistorialConsulta_Load(object sender, EventArgs e)
        {
            string codigo = PropidadesVariables.CurrentPacienteHistorial;

            TxtCodigo.Text = codigo;

            var nombrePaciente = DB.Pacientes.Where(x => x.CodigoPaciente == codigo).Single();

            var consultas = (from c in DB.Consultas 
                             where c.CodigoPaciente == codigo 
                             select new 
                             {
                                 c.ID,
                                 c.FechaConsulta,
                                 c.MotivoConsulta,
                                 c.Antecedentes,
                                 c.Diagnostico,
                                 c.Tratamiento
                             }).OrderByDescending(x => x.ID).ToList();

            if (nombrePaciente != null && !String.IsNullOrWhiteSpace(nombrePaciente.Nombre.ToString()))
            {
                TxtNombre.Text = nombrePaciente.Nombre.ToString();
            }

            if (consultas.Count > 0)
            {
                DgHistorial.DataSource = consultas;
            }
            else
            {
                DgHistorial.DataSource = consultas;
                MessageBox.Show("No existe historial para este paciente en la base de datos. Favor verificar.", "Sistema Fundación Optimedica", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void BtnCerrar_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void BtnMinimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void DgHistorial_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            PropidadesVariables.ConsultaIDDetalle = Convert.ToInt32(DgHistorial.CurrentRow.Cells[0].Value.ToString());

            if(PropidadesVariables.ConsultaIDDetalle > 0)
            {
                DetalleConsulta detalle = new DetalleConsulta();
                detalle.Show();
            }
        }
    }
}
