﻿using Clinica.Clases;
using Clinica.Datos.BBDD;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Clinica.Formularios.HistorialConsultas
{
    public partial class DetalleConsulta : Form
    {
        public DetalleConsulta()
        {
            InitializeComponent();
        }

        OftalmologiaEntities DB = new OftalmologiaEntities();

        private void DetalleConsulta_Load(object sender, EventArgs e)
        {
            var ultimaConsulta = DB.Consultas.SingleOrDefault(x => x.ID == PropidadesVariables.ConsultaIDDetalle);

            if (ultimaConsulta != null)
            {
                TxtUltimaConsulta.Text = ultimaConsulta.MotivoConsulta;
                TxtAntecedentes.Text = ultimaConsulta.Antecedentes;
                TxtLAOD.Text = ultimaConsulta.LAOD;
                TxtLAOI.Text = ultimaConsulta.LAOI;
                TxtLADIP.Text = ultimaConsulta.LADIP;
                TxtLAADD.Text = ultimaConsulta.LAADD;
                TxtInspeccionFisica.Text = ultimaConsulta.InspeccionFisica;
                TxtParpados.Text = ultimaConsulta.Parpados;
                TxtConjuntivas.Text = ultimaConsulta.Conjuntivas;
                TxtCorneas.Text = ultimaConsulta.Corneas;
                TxtIris.Text = ultimaConsulta.Iris;
                TxtPupilas.Text = ultimaConsulta.Pupilas;
                TxtSCOD.Text = ultimaConsulta.SCOD;
                TxtSCOI.Text = ultimaConsulta.SCOI;
                TxtSCJ.Text = ultimaConsulta.SCJ;
                TxtSSOD.Text = ultimaConsulta.SSOD;
                TxtSSOI.Text = ultimaConsulta.SSOI;
                TxtSSJ.Text = ultimaConsulta.SSJ;
                TxtPIOI.Text = ultimaConsulta.PIOI;
                TxtPIOD.Text = ultimaConsulta.PIOD;
                TxtFondoscopia.Text = ultimaConsulta.Fondoscopia;
                TxtBiomicrospia.Text = ultimaConsulta.Biomicrospia;
                TxtDiagnostico.Text = ultimaConsulta.Diagnostico;
                TxtPlan.Text = ultimaConsulta.Plann;
                TxtTratamiento.Text = ultimaConsulta.Tratamiento;
                TxtAROD.Text = ultimaConsulta.AROD;
                TxtAROI.Text = ultimaConsulta.AROI;
                TxtARDIP.Text = ultimaConsulta.ARDIP;
                if (ultimaConsulta.FechaConsulta == null)
                {
                    dtpFechaConsulta.Value = DateTime.Now;
                }
                else
                {
                    dtpFechaConsulta.Value = ultimaConsulta.FechaConsulta.Value;
                }
            }
        }

        private void BtnCerrar_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void BtnMinimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
    }
}
