﻿using Clinica.Clases;
using Clinica.Datos.BBDD;
using Clinica.Formularios.CRUD_GestionUsuarios;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Clinica.Formularios
{
    public partial class GestionUsuarios : Form
    {
        public GestionUsuarios()
        {
            InitializeComponent();
        }
        OftalmologiaEntities DB = new OftalmologiaEntities();

        private void BtnAddUser_Click_1(object sender, EventArgs e)
        {
            NuevoUsuario nuevo = new NuevoUsuario();
            nuevo.Show();
        }

        private void BtnEditUser_Click_1(object sender, EventArgs e)
        {
            if(DgUsuarios.Visible)
            {
                PropidadesVariables.EditUser = DgUsuarios.CurrentRow.Cells[3].Value.ToString();
                EditarUsuario editar = new EditarUsuario();
                editar.Show();
            }
            else
            {
                MessageBox.Show("No se ha seleccionado ningún usuario para editar.", "Sistema Fundación Optimedica ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void BtnDeleteUser_Click(object sender, EventArgs e)
        {
            if (!DgUsuarios.Visible)
            {
                MessageBox.Show("No se ha seleccionado ningún usuario para borrar.", "Sistema Fundación Optimedica ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                DialogResult result = MessageBox.Show("¿Está seguro de eliminar este usuario?", "Confirmación", MessageBoxButtons.YesNo);
                if (result == DialogResult.Yes)
                {
                    try
                    {
                        PropidadesVariables.DeleteUser = DgUsuarios.CurrentRow.Cells[1].Value.ToString();

                        var usuario = DB.Usuarios.SingleOrDefault(x => x.Username == PropidadesVariables.DeleteUser);

                        if (usuario != null)
                        {
                            usuario.Estado = false;

                            DB.SaveChanges();
                            DgUsuarios.DataSource = DB.Usuarios.ToList();

                            MessageBox.Show("\nUsuario eliminado correctamente.\n", "Sistema Fundación Optimedica ", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show($"Ha sucedido un error. \n {ex.Message}");
                    }
                }
            }
        }

        private void BtnCambiarContrasena_Click_1(object sender, EventArgs e)
        {
            if(DgUsuarios.Visible)
            {
                PropidadesVariables.ChangePasswordUsername = DgUsuarios.CurrentRow.Cells[3].Value.ToString();
                CambiarContrasena cambiar = new CambiarContrasena();
                cambiar.Show();
            }
            else
            {
                MessageBox.Show("No se ha seleccionado ningún usuario para cambiar contraseña.", "Sistema Fundación Optimedica ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void BtnBuscar_Click(object sender, EventArgs e)
        {
            var usuario = new object();

            if(!String.IsNullOrWhiteSpace(TxtNombre.Text))
            {
                usuario = (from u in DB.Usuarios 
                           join r in DB.Roles 
                           on u.IdRol equals r.IdRol
                           where u.Nombres == TxtNombre.Text
                           select new
                           {
                               u.Username,
                               u.Nombres,
                               u.Apellidos,
                               r.Descripcion,
                               u.Estado,
                               u.FechaCreacion,
                               u.FechaModificacion
                           }).ToList();
            }
            else if (!String.IsNullOrWhiteSpace(TxtUsername.Text))
            {
                usuario = (from u in DB.Usuarios
                           join r in DB.Roles
                           on u.IdRol equals r.IdRol
                           where u.Username == TxtUsername.Text
                           select new
                           {
                               u.Username,
                               u.Nombres,
                               u.Apellidos,
                               r.Descripcion,
                               u.Estado,
                               u.FechaCreacion,
                               u.FechaModificacion
                           }).ToList(); ;
            }
            else if (!String.IsNullOrWhiteSpace(TxtApellido.Text))
            {
                usuario = (from u in DB.Usuarios
                           join r in DB.Roles
                           on u.IdRol equals r.IdRol
                           where u.Apellidos == TxtApellido.Text
                           select new
                           {
                               u.Username,
                               u.Nombres,
                               u.Apellidos,
                               r.Descripcion,
                               u.Estado,
                               u.FechaCreacion,
                               u.FechaModificacion
                           }).ToList();
                
            }
            else
            {
                usuario = (from u in DB.Usuarios
                           join r in DB.Roles
                           on u.IdRol equals r.IdRol
                           select new
                           {
                               u.Username,
                               u.Nombres,
                               u.Apellidos,
                               r.Descripcion,
                               u.Estado,
                               u.FechaCreacion,
                               u.FechaModificacion
                           }).ToList();
            }

            if (usuario != null)
            {
                DgUsuarios.DataSource = usuario;
                DgUsuarios.Columns[0].Width = 250;
                DgUsuarios.Columns[1].Width = 250;
                DgUsuarios.Visible = true;
            }
            else
            {
                MessageBox.Show("\nUsuario no encontrado. Favor verificar.\n", "Sistema Fundación Optimedica ", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}
