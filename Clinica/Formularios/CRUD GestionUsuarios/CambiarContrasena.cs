﻿using Clinica.Clases;
using Clinica.Datos.BBDD;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Clinica.Formularios.CRUD_GestionUsuarios
{
    public partial class CambiarContrasena : Form
    {
        public CambiarContrasena()
        {
            InitializeComponent();
        }

        OftalmologiaEntities DB = new OftalmologiaEntities();

        private void CambiarContrasena_Load(object sender, EventArgs e)
        {
            TxtUsername.Text = PropidadesVariables.ChangePasswordUsername;
        }
        private void BtnIngresar_Click(object sender, EventArgs e)
        {
            try
            {
                string username = TxtUsername.Text;
                var usuario = DB.Usuarios.SingleOrDefault(x => x.Username == username);

                if(usuario != null)
                {
                    if(TxtPassword.Text == TxtConfirmaPassword.Text)
                    {
                        usuario.Password = TxtPassword.Text;
                        DB.SaveChanges();
                    }
                    else
                    {
                        MessageBox.Show("La contraseña y la confirmación no coinciden. Favor verificar.", "Sistema Fundación Optimedica", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
                else
                {
                    MessageBox.Show("Usuario no encontrado. Favor verificar.", "Sistema Fundación Optimedica", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Ha sucedido un error. \n {ex.Message}");
            }
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            TxtUsername.Clear();
            TxtPassword.Clear();
            TxtConfirmaPassword.Clear();
            this.Hide();
        }
    }
}
