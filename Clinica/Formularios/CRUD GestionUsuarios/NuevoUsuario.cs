﻿using Clinica.Datos.BBDD;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Clinica.Formularios.CRUD_GestionUsuarios
{
    public partial class NuevoUsuario : Form
    {
        public NuevoUsuario()
        {
            InitializeComponent();
        }
        OftalmologiaEntities DB = new OftalmologiaEntities();

        private void NuevoUsuario_Load(object sender, EventArgs e)
        {
            var roles = DB.Roles.ToList();

            CmbRol.DataSource = roles;
            CmbRol.DisplayMember = "Descripcion";
            CmbRol.ValueMember = "IdRol";
        }

        private void BtnIngresar_Click(object sender, EventArgs e)
        {
            try
            {
                var verificacionUsuario = DB.Usuarios.SingleOrDefault(x => x.Username == TxtUsername.Text);

                if (verificacionUsuario != null)
                {
                    MessageBox.Show("Este usuario ya existe, favor verificar.", "Sistema Fundación Optimedica", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                else
                {
                    if (TxtContraseña.Text == TxtConfirmaContraseña.Text)
                    {
                        DB.Usuarios.Add(new Usuarios
                        {
                            Nombres = TxtNombres.Text,
                            Apellidos = TxtApellidos.Text,
                            Password = TxtContraseña.Text,
                            IdRol = Convert.ToInt32(CmbRol.SelectedValue.ToString()),
                            Estado = true,
                            FechaCreacion = DateTime.Now
                        });
                        DB.SaveChanges();
                    }
                    else
                    {
                        MessageBox.Show("La contraseña y la confirmación no coinciden. Favor verificar.", "Sistema Fundación Optimedica", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    this.Hide();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Ha sucedido un error. \n {ex.Message}");
            }
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            TxtUsername.Clear();
            TxtNombres.Clear();
            TxtApellidos.Clear();
            TxtContraseña.Clear();
            TxtConfirmaContraseña.Clear();
            this.Hide();
        }
    }
}
