﻿using Clinica.Clases;
using Clinica.Datos.BBDD;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Clinica.Formularios.CRUD_GestionUsuarios
{
    public partial class EditarUsuario : Form
    {
        public EditarUsuario()
        {
            InitializeComponent();
        }
        OftalmologiaEntities DB = new OftalmologiaEntities();
        string username = PropidadesVariables.EditUser;
        private void EditarUsuario_Load(object sender, EventArgs e)
        {
            var roles = DB.Roles.ToList();

            CmbRol.DataSource = roles;
            CmbRol.DisplayMember = "Descripcion";
            CmbRol.ValueMember = "IdRol";

            if(username != null)
            {
                var user = DB.Usuarios.Where(x => x.Username == username).SingleOrDefault();

                if(user != null)
                {
                    TxtNombres.Text = user.Nombres;
                    TxtApellidos.Text = user.Apellidos;
                    CmbRol.SelectedValue = user.IdRol;
                }
            }    
        }

        private void BtnIngresar_Click(object sender, EventArgs e)
        {
            try
            {
                var user = DB.Usuarios.Where(x => x.Username == username).SingleOrDefault();

                if (user != null)
                {
                    user.Nombres = TxtNombres.Text;
                    user.Apellidos = TxtApellidos.Text;
                    user.IdRol = Convert.ToInt32(CmbRol.SelectedValue.ToString());
                    DB.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Ha sucedido un error. \n {ex.Message}");
            }
        }
    }
}
