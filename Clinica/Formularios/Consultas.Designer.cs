﻿namespace Clinica.Formularios
{
    partial class Consultas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Consultas));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.nuevaConsultaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.historialToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.BtnCancelar = new System.Windows.Forms.Button();
            this.BtnGuardar = new System.Windows.Forms.Button();
            this.dtpFechaConsulta = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtCodigo = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.LblMotivoConsulta = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.LblLentes = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.TxtNombre = new System.Windows.Forms.TextBox();
            this.TxtEdad = new System.Windows.Forms.TextBox();
            this.TxtSexo = new System.Windows.Forms.TextBox();
            this.TxtUltimaConsulta = new System.Windows.Forms.TextBox();
            this.TxtAntecedentes = new System.Windows.Forms.TextBox();
            this.TxtLAOD = new System.Windows.Forms.TextBox();
            this.TxtLAOI = new System.Windows.Forms.TextBox();
            this.TxtLADIP = new System.Windows.Forms.TextBox();
            this.TxtLAADD = new System.Windows.Forms.TextBox();
            this.TxtInspeccionFisica = new System.Windows.Forms.TextBox();
            this.TxtParpados = new System.Windows.Forms.TextBox();
            this.TxtConjuntivas = new System.Windows.Forms.TextBox();
            this.TxtCorneas = new System.Windows.Forms.TextBox();
            this.TxtIris = new System.Windows.Forms.TextBox();
            this.TxtPupilas = new System.Windows.Forms.TextBox();
            this.TxtSCOD = new System.Windows.Forms.TextBox();
            this.TxtSCOI = new System.Windows.Forms.TextBox();
            this.TxtSCJ = new System.Windows.Forms.TextBox();
            this.TxtSSOD = new System.Windows.Forms.TextBox();
            this.TxtSSOI = new System.Windows.Forms.TextBox();
            this.TxtSSJ = new System.Windows.Forms.TextBox();
            this.TxtPIOD = new System.Windows.Forms.TextBox();
            this.TxtPIOI = new System.Windows.Forms.TextBox();
            this.TxtFondoscopia = new System.Windows.Forms.TextBox();
            this.TxtBiomicrospia = new System.Windows.Forms.TextBox();
            this.TxtDiagnostico = new System.Windows.Forms.TextBox();
            this.TxtPlan = new System.Windows.Forms.TextBox();
            this.TxtTratamiento = new System.Windows.Forms.TextBox();
            this.TxtAROD = new System.Windows.Forms.TextBox();
            this.TxtAROI = new System.Windows.Forms.TextBox();
            this.TxtARDIP = new System.Windows.Forms.TextBox();
            this.TxtRLOD = new System.Windows.Forms.TextBox();
            this.TxtRLOI = new System.Windows.Forms.TextBox();
            this.TxtRLDIP = new System.Windows.Forms.TextBox();
            this.TxtRLADD = new System.Windows.Forms.TextBox();
            this.PbFotoPaciente = new System.Windows.Forms.PictureBox();
            this.BtnBuscar = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PbFotoPaciente)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nuevaConsultaToolStripMenuItem,
            this.historialToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(8, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(1388, 28);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // nuevaConsultaToolStripMenuItem
            // 
            this.nuevaConsultaToolStripMenuItem.Image = global::Clinica.Properties.Resources.icons8_plan_de_tratamiento_32;
            this.nuevaConsultaToolStripMenuItem.Name = "nuevaConsultaToolStripMenuItem";
            this.nuevaConsultaToolStripMenuItem.Size = new System.Drawing.Size(144, 24);
            this.nuevaConsultaToolStripMenuItem.Text = "Nueva consulta";
            this.nuevaConsultaToolStripMenuItem.Click += new System.EventHandler(this.nuevaConsultaToolStripMenuItem_Click);
            // 
            // historialToolStripMenuItem
            // 
            this.historialToolStripMenuItem.Image = global::Clinica.Properties.Resources.icons8_pasado_32;
            this.historialToolStripMenuItem.Name = "historialToolStripMenuItem";
            this.historialToolStripMenuItem.Size = new System.Drawing.Size(99, 24);
            this.historialToolStripMenuItem.Text = "Historial";
            this.historialToolStripMenuItem.Click += new System.EventHandler(this.historialToolStripMenuItem_Click);
            // 
            // BtnCancelar
            // 
            this.BtnCancelar.Image = global::Clinica.Properties.Resources.icons8_eliminar_archivo_32;
            this.BtnCancelar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.BtnCancelar.Location = new System.Drawing.Point(1207, 78);
            this.BtnCancelar.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.BtnCancelar.Name = "BtnCancelar";
            this.BtnCancelar.Size = new System.Drawing.Size(91, 73);
            this.BtnCancelar.TabIndex = 153;
            this.BtnCancelar.Text = "Cancelar";
            this.BtnCancelar.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.BtnCancelar.UseVisualStyleBackColor = true;
            this.BtnCancelar.Click += new System.EventHandler(this.BtnCancelar_Click_1);
            // 
            // BtnGuardar
            // 
            this.BtnGuardar.Image = global::Clinica.Properties.Resources.icons8_agregar_archivo_32;
            this.BtnGuardar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.BtnGuardar.Location = new System.Drawing.Point(1108, 78);
            this.BtnGuardar.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.BtnGuardar.Name = "BtnGuardar";
            this.BtnGuardar.Size = new System.Drawing.Size(91, 73);
            this.BtnGuardar.TabIndex = 152;
            this.BtnGuardar.Text = "Guardar";
            this.BtnGuardar.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.BtnGuardar.UseVisualStyleBackColor = true;
            this.BtnGuardar.Click += new System.EventHandler(this.BtnGuardar_Click_1);
            // 
            // dtpFechaConsulta
            // 
            this.dtpFechaConsulta.Enabled = false;
            this.dtpFechaConsulta.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpFechaConsulta.Location = new System.Drawing.Point(1148, 33);
            this.dtpFechaConsulta.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dtpFechaConsulta.Name = "dtpFechaConsulta";
            this.dtpFechaConsulta.Size = new System.Drawing.Size(148, 22);
            this.dtpFechaConsulta.TabIndex = 110;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(213, 85);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 17);
            this.label2.TabIndex = 109;
            this.label2.Text = "Codigo";
            // 
            // TxtCodigo
            // 
            this.TxtCodigo.Location = new System.Drawing.Point(319, 81);
            this.TxtCodigo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TxtCodigo.Name = "TxtCodigo";
            this.TxtCodigo.Size = new System.Drawing.Size(157, 22);
            this.TxtCodigo.TabIndex = 108;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(75, 907);
            this.label31.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(34, 17);
            this.label31.TabIndex = 158;
            this.label31.Text = "DIP:";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(1040, 933);
            this.label36.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(41, 17);
            this.label36.TabIndex = 167;
            this.label36.Text = "ADD:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(213, 117);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 17);
            this.label1.TabIndex = 107;
            this.label1.Text = "Nombre:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(676, 117);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 17);
            this.label3.TabIndex = 112;
            this.label3.Text = "Edad:";
            // 
            // LblMotivoConsulta
            // 
            this.LblMotivoConsulta.AutoSize = true;
            this.LblMotivoConsulta.Location = new System.Drawing.Point(61, 145);
            this.LblMotivoConsulta.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LblMotivoConsulta.Name = "LblMotivoConsulta";
            this.LblMotivoConsulta.Size = new System.Drawing.Size(144, 17);
            this.LblMotivoConsulta.TabIndex = 114;
            this.LblMotivoConsulta.Text = "Motivo útima consulta";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(65, 235);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(99, 17);
            this.label5.TabIndex = 115;
            this.label5.Text = "Antecedentes:";
            // 
            // LblLentes
            // 
            this.LblLentes.AutoSize = true;
            this.LblLentes.Location = new System.Drawing.Point(65, 268);
            this.LblLentes.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LblLentes.Name = "LblLentes";
            this.LblLentes.Size = new System.Drawing.Size(127, 17);
            this.LblLentes.TabIndex = 116;
            this.LblLentes.Text = "Lentes anteriores: ";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(285, 268);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(33, 17);
            this.label7.TabIndex = 118;
            this.label7.Text = "OD:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(543, 268);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(26, 17);
            this.label8.TabIndex = 119;
            this.label8.Text = "OI:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(65, 300);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(115, 17);
            this.label9.TabIndex = 121;
            this.label9.Text = "Inspección física:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(65, 332);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(73, 17);
            this.label10.TabIndex = 123;
            this.label10.Text = "Parpados:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(65, 460);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(58, 17);
            this.label14.TabIndex = 125;
            this.label14.Text = "Pupilas:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(65, 428);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(30, 17);
            this.label15.TabIndex = 127;
            this.label15.Text = "Iris:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(65, 396);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(65, 17);
            this.label16.TabIndex = 129;
            this.label16.Text = "Corneas:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(65, 364);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(85, 17);
            this.label17.TabIndex = 131;
            this.label17.Text = "Conjuntivas:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(65, 492);
            this.label20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(54, 17);
            this.label20.TabIndex = 133;
            this.label20.Text = "Vision: ";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(68, 590);
            this.label24.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(89, 17);
            this.label24.TabIndex = 141;
            this.label24.Text = "Fondoscopia";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(707, 590);
            this.label25.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(88, 17);
            this.label25.TabIndex = 142;
            this.label25.Text = "Biomicrospia";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(68, 660);
            this.label27.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(82, 17);
            this.label27.TabIndex = 144;
            this.label27.Text = "Diagnostico";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(707, 660);
            this.label26.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(36, 17);
            this.label26.TabIndex = 146;
            this.label26.Text = "Plan";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(72, 746);
            this.label28.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(88, 17);
            this.label28.TabIndex = 147;
            this.label28.Text = "Tratamiento:";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(67, 814);
            this.label29.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(86, 17);
            this.label29.TabIndex = 149;
            this.label29.Text = "Autorefracto";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(833, 117);
            this.label30.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(43, 17);
            this.label30.TabIndex = 151;
            this.label30.Text = "Sexo:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(73, 843);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(33, 17);
            this.label6.TabIndex = 154;
            this.label6.Text = "OD:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(75, 875);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(26, 17);
            this.label4.TabIndex = 155;
            this.label4.Text = "OI:";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(1032, 807);
            this.label35.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(100, 17);
            this.label35.TabIndex = 160;
            this.label35.Text = "Receta Lentes";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(1039, 837);
            this.label34.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(33, 17);
            this.label34.TabIndex = 161;
            this.label34.Text = "OD:";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(1040, 869);
            this.label33.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(26, 17);
            this.label33.TabIndex = 162;
            this.label33.Text = "OI:";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(1040, 901);
            this.label32.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(34, 17);
            this.label32.TabIndex = 165;
            this.label32.Text = "DIP:";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(293, 492);
            this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(30, 17);
            this.label19.TabIndex = 134;
            this.label19.Text = "SC:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(293, 530);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(30, 17);
            this.label18.TabIndex = 135;
            this.label18.Text = "SS:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(1025, 528);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(33, 17);
            this.label12.TabIndex = 136;
            this.label12.Text = "OD:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(1025, 560);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(26, 17);
            this.label11.TabIndex = 138;
            this.label11.Text = "OI:";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(333, 492);
            this.label38.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(33, 17);
            this.label38.TabIndex = 169;
            this.label38.Text = "OD:";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(471, 492);
            this.label37.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(26, 17);
            this.label37.TabIndex = 170;
            this.label37.Text = "OI:";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(604, 492);
            this.label39.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(19, 17);
            this.label39.TabIndex = 172;
            this.label39.Text = "J:";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(333, 530);
            this.label42.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(33, 17);
            this.label42.TabIndex = 175;
            this.label42.Text = "OD:";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(471, 530);
            this.label41.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(26, 17);
            this.label41.TabIndex = 176;
            this.label41.Text = "OI:";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(604, 530);
            this.label40.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(19, 17);
            this.label40.TabIndex = 178;
            this.label40.Text = "J:";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(1023, 495);
            this.label43.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(135, 17);
            this.label43.TabIndex = 181;
            this.label43.Text = "Presión Intraocular: ";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(788, 268);
            this.label21.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(34, 17);
            this.label21.TabIndex = 183;
            this.label21.Text = "DIP:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(1045, 268);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(41, 17);
            this.label13.TabIndex = 184;
            this.label13.Text = "ADD:";
            // 
            // TxtNombre
            // 
            this.TxtNombre.Location = new System.Drawing.Point(319, 117);
            this.TxtNombre.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TxtNombre.Name = "TxtNombre";
            this.TxtNombre.ReadOnly = true;
            this.TxtNombre.Size = new System.Drawing.Size(331, 22);
            this.TxtNombre.TabIndex = 185;
            // 
            // TxtEdad
            // 
            this.TxtEdad.Location = new System.Drawing.Point(731, 117);
            this.TxtEdad.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TxtEdad.Name = "TxtEdad";
            this.TxtEdad.ReadOnly = true;
            this.TxtEdad.Size = new System.Drawing.Size(93, 22);
            this.TxtEdad.TabIndex = 186;
            // 
            // TxtSexo
            // 
            this.TxtSexo.Location = new System.Drawing.Point(883, 117);
            this.TxtSexo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TxtSexo.Name = "TxtSexo";
            this.TxtSexo.ReadOnly = true;
            this.TxtSexo.Size = new System.Drawing.Size(93, 22);
            this.TxtSexo.TabIndex = 187;
            // 
            // TxtUltimaConsulta
            // 
            this.TxtUltimaConsulta.Location = new System.Drawing.Point(65, 165);
            this.TxtUltimaConsulta.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TxtUltimaConsulta.Multiline = true;
            this.TxtUltimaConsulta.Name = "TxtUltimaConsulta";
            this.TxtUltimaConsulta.ReadOnly = true;
            this.TxtUltimaConsulta.Size = new System.Drawing.Size(1231, 50);
            this.TxtUltimaConsulta.TabIndex = 188;
            // 
            // TxtAntecedentes
            // 
            this.TxtAntecedentes.Location = new System.Drawing.Point(289, 231);
            this.TxtAntecedentes.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TxtAntecedentes.Name = "TxtAntecedentes";
            this.TxtAntecedentes.ReadOnly = true;
            this.TxtAntecedentes.Size = new System.Drawing.Size(1007, 22);
            this.TxtAntecedentes.TabIndex = 189;
            // 
            // TxtLAOD
            // 
            this.TxtLAOD.Location = new System.Drawing.Point(325, 265);
            this.TxtLAOD.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TxtLAOD.Name = "TxtLAOD";
            this.TxtLAOD.ReadOnly = true;
            this.TxtLAOD.Size = new System.Drawing.Size(172, 22);
            this.TxtLAOD.TabIndex = 190;
            // 
            // TxtLAOI
            // 
            this.TxtLAOI.Location = new System.Drawing.Point(579, 265);
            this.TxtLAOI.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TxtLAOI.Name = "TxtLAOI";
            this.TxtLAOI.ReadOnly = true;
            this.TxtLAOI.Size = new System.Drawing.Size(172, 22);
            this.TxtLAOI.TabIndex = 191;
            // 
            // TxtLADIP
            // 
            this.TxtLADIP.Location = new System.Drawing.Point(833, 265);
            this.TxtLADIP.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TxtLADIP.Name = "TxtLADIP";
            this.TxtLADIP.ReadOnly = true;
            this.TxtLADIP.Size = new System.Drawing.Size(172, 22);
            this.TxtLADIP.TabIndex = 192;
            // 
            // TxtLAADD
            // 
            this.TxtLAADD.Location = new System.Drawing.Point(1097, 265);
            this.TxtLAADD.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TxtLAADD.Name = "TxtLAADD";
            this.TxtLAADD.ReadOnly = true;
            this.TxtLAADD.Size = new System.Drawing.Size(172, 22);
            this.TxtLAADD.TabIndex = 193;
            // 
            // TxtInspeccionFisica
            // 
            this.TxtInspeccionFisica.Location = new System.Drawing.Point(289, 297);
            this.TxtInspeccionFisica.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TxtInspeccionFisica.Name = "TxtInspeccionFisica";
            this.TxtInspeccionFisica.ReadOnly = true;
            this.TxtInspeccionFisica.Size = new System.Drawing.Size(1007, 22);
            this.TxtInspeccionFisica.TabIndex = 194;
            // 
            // TxtParpados
            // 
            this.TxtParpados.Location = new System.Drawing.Point(289, 329);
            this.TxtParpados.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TxtParpados.Name = "TxtParpados";
            this.TxtParpados.ReadOnly = true;
            this.TxtParpados.Size = new System.Drawing.Size(1007, 22);
            this.TxtParpados.TabIndex = 195;
            // 
            // TxtConjuntivas
            // 
            this.TxtConjuntivas.Location = new System.Drawing.Point(289, 361);
            this.TxtConjuntivas.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TxtConjuntivas.Name = "TxtConjuntivas";
            this.TxtConjuntivas.ReadOnly = true;
            this.TxtConjuntivas.Size = new System.Drawing.Size(1007, 22);
            this.TxtConjuntivas.TabIndex = 196;
            // 
            // TxtCorneas
            // 
            this.TxtCorneas.Location = new System.Drawing.Point(289, 393);
            this.TxtCorneas.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TxtCorneas.Name = "TxtCorneas";
            this.TxtCorneas.ReadOnly = true;
            this.TxtCorneas.Size = new System.Drawing.Size(1007, 22);
            this.TxtCorneas.TabIndex = 197;
            // 
            // TxtIris
            // 
            this.TxtIris.Location = new System.Drawing.Point(289, 425);
            this.TxtIris.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TxtIris.Name = "TxtIris";
            this.TxtIris.ReadOnly = true;
            this.TxtIris.Size = new System.Drawing.Size(1007, 22);
            this.TxtIris.TabIndex = 198;
            // 
            // TxtPupilas
            // 
            this.TxtPupilas.Location = new System.Drawing.Point(289, 457);
            this.TxtPupilas.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TxtPupilas.Name = "TxtPupilas";
            this.TxtPupilas.ReadOnly = true;
            this.TxtPupilas.Size = new System.Drawing.Size(1007, 22);
            this.TxtPupilas.TabIndex = 199;
            // 
            // TxtSCOD
            // 
            this.TxtSCOD.Location = new System.Drawing.Point(376, 489);
            this.TxtSCOD.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TxtSCOD.Name = "TxtSCOD";
            this.TxtSCOD.ReadOnly = true;
            this.TxtSCOD.Size = new System.Drawing.Size(85, 22);
            this.TxtSCOD.TabIndex = 200;
            // 
            // TxtSCOI
            // 
            this.TxtSCOI.Location = new System.Drawing.Point(507, 489);
            this.TxtSCOI.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TxtSCOI.Name = "TxtSCOI";
            this.TxtSCOI.ReadOnly = true;
            this.TxtSCOI.Size = new System.Drawing.Size(85, 22);
            this.TxtSCOI.TabIndex = 201;
            // 
            // TxtSCJ
            // 
            this.TxtSCJ.Location = new System.Drawing.Point(632, 489);
            this.TxtSCJ.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TxtSCJ.Name = "TxtSCJ";
            this.TxtSCJ.ReadOnly = true;
            this.TxtSCJ.Size = new System.Drawing.Size(85, 22);
            this.TxtSCJ.TabIndex = 202;
            // 
            // TxtSSOD
            // 
            this.TxtSSOD.Location = new System.Drawing.Point(376, 527);
            this.TxtSSOD.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TxtSSOD.Name = "TxtSSOD";
            this.TxtSSOD.ReadOnly = true;
            this.TxtSSOD.Size = new System.Drawing.Size(85, 22);
            this.TxtSSOD.TabIndex = 203;
            // 
            // TxtSSOI
            // 
            this.TxtSSOI.Location = new System.Drawing.Point(507, 527);
            this.TxtSSOI.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TxtSSOI.Name = "TxtSSOI";
            this.TxtSSOI.ReadOnly = true;
            this.TxtSSOI.Size = new System.Drawing.Size(85, 22);
            this.TxtSSOI.TabIndex = 204;
            // 
            // TxtSSJ
            // 
            this.TxtSSJ.Location = new System.Drawing.Point(632, 527);
            this.TxtSSJ.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TxtSSJ.Name = "TxtSSJ";
            this.TxtSSJ.ReadOnly = true;
            this.TxtSSJ.Size = new System.Drawing.Size(85, 22);
            this.TxtSSJ.TabIndex = 205;
            // 
            // TxtPIOD
            // 
            this.TxtPIOD.Location = new System.Drawing.Point(1081, 524);
            this.TxtPIOD.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TxtPIOD.Name = "TxtPIOD";
            this.TxtPIOD.ReadOnly = true;
            this.TxtPIOD.Size = new System.Drawing.Size(172, 22);
            this.TxtPIOD.TabIndex = 206;
            // 
            // TxtPIOI
            // 
            this.TxtPIOI.Location = new System.Drawing.Point(1081, 556);
            this.TxtPIOI.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TxtPIOI.Name = "TxtPIOI";
            this.TxtPIOI.ReadOnly = true;
            this.TxtPIOI.Size = new System.Drawing.Size(172, 22);
            this.TxtPIOI.TabIndex = 207;
            // 
            // TxtFondoscopia
            // 
            this.TxtFondoscopia.Location = new System.Drawing.Point(72, 606);
            this.TxtFondoscopia.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TxtFondoscopia.Multiline = true;
            this.TxtFondoscopia.Name = "TxtFondoscopia";
            this.TxtFondoscopia.ReadOnly = true;
            this.TxtFondoscopia.Size = new System.Drawing.Size(600, 50);
            this.TxtFondoscopia.TabIndex = 208;
            // 
            // TxtBiomicrospia
            // 
            this.TxtBiomicrospia.Location = new System.Drawing.Point(711, 606);
            this.TxtBiomicrospia.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TxtBiomicrospia.Multiline = true;
            this.TxtBiomicrospia.Name = "TxtBiomicrospia";
            this.TxtBiomicrospia.ReadOnly = true;
            this.TxtBiomicrospia.Size = new System.Drawing.Size(585, 50);
            this.TxtBiomicrospia.TabIndex = 209;
            // 
            // TxtDiagnostico
            // 
            this.TxtDiagnostico.Location = new System.Drawing.Point(72, 679);
            this.TxtDiagnostico.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TxtDiagnostico.Multiline = true;
            this.TxtDiagnostico.Name = "TxtDiagnostico";
            this.TxtDiagnostico.ReadOnly = true;
            this.TxtDiagnostico.Size = new System.Drawing.Size(600, 50);
            this.TxtDiagnostico.TabIndex = 210;
            // 
            // TxtPlan
            // 
            this.TxtPlan.Location = new System.Drawing.Point(711, 679);
            this.TxtPlan.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TxtPlan.Multiline = true;
            this.TxtPlan.Name = "TxtPlan";
            this.TxtPlan.ReadOnly = true;
            this.TxtPlan.Size = new System.Drawing.Size(585, 50);
            this.TxtPlan.TabIndex = 211;
            // 
            // TxtTratamiento
            // 
            this.TxtTratamiento.Location = new System.Drawing.Point(289, 742);
            this.TxtTratamiento.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TxtTratamiento.Name = "TxtTratamiento";
            this.TxtTratamiento.ReadOnly = true;
            this.TxtTratamiento.Size = new System.Drawing.Size(1007, 22);
            this.TxtTratamiento.TabIndex = 212;
            // 
            // TxtAROD
            // 
            this.TxtAROD.Location = new System.Drawing.Point(116, 839);
            this.TxtAROD.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TxtAROD.Name = "TxtAROD";
            this.TxtAROD.ReadOnly = true;
            this.TxtAROD.Size = new System.Drawing.Size(172, 22);
            this.TxtAROD.TabIndex = 213;
            // 
            // TxtAROI
            // 
            this.TxtAROI.Location = new System.Drawing.Point(116, 871);
            this.TxtAROI.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TxtAROI.Name = "TxtAROI";
            this.TxtAROI.ReadOnly = true;
            this.TxtAROI.Size = new System.Drawing.Size(172, 22);
            this.TxtAROI.TabIndex = 214;
            // 
            // TxtARDIP
            // 
            this.TxtARDIP.Location = new System.Drawing.Point(116, 903);
            this.TxtARDIP.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TxtARDIP.Name = "TxtARDIP";
            this.TxtARDIP.ReadOnly = true;
            this.TxtARDIP.Size = new System.Drawing.Size(172, 22);
            this.TxtARDIP.TabIndex = 215;
            // 
            // TxtRLOD
            // 
            this.TxtRLOD.Location = new System.Drawing.Point(1081, 833);
            this.TxtRLOD.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TxtRLOD.Name = "TxtRLOD";
            this.TxtRLOD.ReadOnly = true;
            this.TxtRLOD.Size = new System.Drawing.Size(172, 22);
            this.TxtRLOD.TabIndex = 216;
            // 
            // TxtRLOI
            // 
            this.TxtRLOI.Location = new System.Drawing.Point(1081, 865);
            this.TxtRLOI.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TxtRLOI.Name = "TxtRLOI";
            this.TxtRLOI.ReadOnly = true;
            this.TxtRLOI.Size = new System.Drawing.Size(172, 22);
            this.TxtRLOI.TabIndex = 217;
            // 
            // TxtRLDIP
            // 
            this.TxtRLDIP.Location = new System.Drawing.Point(1081, 897);
            this.TxtRLDIP.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TxtRLDIP.Name = "TxtRLDIP";
            this.TxtRLDIP.ReadOnly = true;
            this.TxtRLDIP.Size = new System.Drawing.Size(172, 22);
            this.TxtRLDIP.TabIndex = 218;
            // 
            // TxtRLADD
            // 
            this.TxtRLADD.Location = new System.Drawing.Point(1081, 929);
            this.TxtRLADD.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TxtRLADD.Name = "TxtRLADD";
            this.TxtRLADD.ReadOnly = true;
            this.TxtRLADD.Size = new System.Drawing.Size(172, 22);
            this.TxtRLADD.TabIndex = 219;
            // 
            // PbFotoPaciente
            // 
            this.PbFotoPaciente.Location = new System.Drawing.Point(65, 32);
            this.PbFotoPaciente.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.PbFotoPaciente.Name = "PbFotoPaciente";
            this.PbFotoPaciente.Size = new System.Drawing.Size(125, 110);
            this.PbFotoPaciente.TabIndex = 220;
            this.PbFotoPaciente.TabStop = false;
            // 
            // BtnBuscar
            // 
            this.BtnBuscar.AutoSize = true;
            this.BtnBuscar.FlatAppearance.BorderSize = 0;
            this.BtnBuscar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnBuscar.Image = global::Clinica.Properties.Resources.icons8_encuentra_hombre_usuario_321;
            this.BtnBuscar.Location = new System.Drawing.Point(507, 63);
            this.BtnBuscar.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.BtnBuscar.MaximumSize = new System.Drawing.Size(124, 95);
            this.BtnBuscar.MinimumSize = new System.Drawing.Size(57, 46);
            this.BtnBuscar.Name = "BtnBuscar";
            this.BtnBuscar.Size = new System.Drawing.Size(57, 47);
            this.BtnBuscar.TabIndex = 223;
            this.BtnBuscar.UseVisualStyleBackColor = true;
            this.BtnBuscar.Click += new System.EventHandler(this.BtnBuscar_Click);
            // 
            // Consultas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1388, 1004);
            this.Controls.Add(this.BtnBuscar);
            this.Controls.Add(this.PbFotoPaciente);
            this.Controls.Add(this.TxtRLADD);
            this.Controls.Add(this.TxtRLDIP);
            this.Controls.Add(this.TxtRLOI);
            this.Controls.Add(this.TxtRLOD);
            this.Controls.Add(this.TxtARDIP);
            this.Controls.Add(this.TxtAROI);
            this.Controls.Add(this.TxtAROD);
            this.Controls.Add(this.TxtTratamiento);
            this.Controls.Add(this.TxtPlan);
            this.Controls.Add(this.TxtDiagnostico);
            this.Controls.Add(this.TxtBiomicrospia);
            this.Controls.Add(this.TxtFondoscopia);
            this.Controls.Add(this.TxtPIOI);
            this.Controls.Add(this.TxtPIOD);
            this.Controls.Add(this.TxtSSJ);
            this.Controls.Add(this.TxtSSOI);
            this.Controls.Add(this.TxtSSOD);
            this.Controls.Add(this.TxtSCJ);
            this.Controls.Add(this.TxtSCOI);
            this.Controls.Add(this.TxtSCOD);
            this.Controls.Add(this.TxtPupilas);
            this.Controls.Add(this.TxtIris);
            this.Controls.Add(this.TxtCorneas);
            this.Controls.Add(this.TxtConjuntivas);
            this.Controls.Add(this.TxtParpados);
            this.Controls.Add(this.TxtInspeccionFisica);
            this.Controls.Add(this.TxtLAADD);
            this.Controls.Add(this.TxtLADIP);
            this.Controls.Add(this.TxtLAOI);
            this.Controls.Add(this.TxtLAOD);
            this.Controls.Add(this.TxtAntecedentes);
            this.Controls.Add(this.TxtUltimaConsulta);
            this.Controls.Add(this.TxtSexo);
            this.Controls.Add(this.TxtEdad);
            this.Controls.Add(this.TxtNombre);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label43);
            this.Controls.Add(this.label40);
            this.Controls.Add(this.label41);
            this.Controls.Add(this.label42);
            this.Controls.Add(this.label39);
            this.Controls.Add(this.label37);
            this.Controls.Add(this.label38);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label36);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.label34);
            this.Controls.Add(this.label35);
            this.Controls.Add(this.label31);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.BtnCancelar);
            this.Controls.Add(this.BtnGuardar);
            this.Controls.Add(this.label30);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.LblLentes);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.LblMotivoConsulta);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.dtpFechaConsulta);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.TxtCodigo);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "Consultas";
            this.Text = "Consultas";
            this.Load += new System.EventHandler(this.Consultas_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PbFotoPaciente)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem historialToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nuevaConsultaToolStripMenuItem;
        private System.Windows.Forms.Button BtnCancelar;
        private System.Windows.Forms.Button BtnGuardar;
        private System.Windows.Forms.DateTimePicker dtpFechaConsulta;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TxtCodigo;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label LblMotivoConsulta;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label LblLentes;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox TxtNombre;
        private System.Windows.Forms.TextBox TxtEdad;
        private System.Windows.Forms.TextBox TxtSexo;
        private System.Windows.Forms.TextBox TxtUltimaConsulta;
        private System.Windows.Forms.TextBox TxtAntecedentes;
        private System.Windows.Forms.TextBox TxtLAOD;
        private System.Windows.Forms.TextBox TxtLAOI;
        private System.Windows.Forms.TextBox TxtLADIP;
        private System.Windows.Forms.TextBox TxtLAADD;
        private System.Windows.Forms.TextBox TxtInspeccionFisica;
        private System.Windows.Forms.TextBox TxtParpados;
        private System.Windows.Forms.TextBox TxtConjuntivas;
        private System.Windows.Forms.TextBox TxtCorneas;
        private System.Windows.Forms.TextBox TxtIris;
        private System.Windows.Forms.TextBox TxtPupilas;
        private System.Windows.Forms.TextBox TxtSCOD;
        private System.Windows.Forms.TextBox TxtSCOI;
        private System.Windows.Forms.TextBox TxtSCJ;
        private System.Windows.Forms.TextBox TxtSSOD;
        private System.Windows.Forms.TextBox TxtSSOI;
        private System.Windows.Forms.TextBox TxtSSJ;
        private System.Windows.Forms.TextBox TxtPIOD;
        private System.Windows.Forms.TextBox TxtPIOI;
        private System.Windows.Forms.TextBox TxtFondoscopia;
        private System.Windows.Forms.TextBox TxtBiomicrospia;
        private System.Windows.Forms.TextBox TxtDiagnostico;
        private System.Windows.Forms.TextBox TxtPlan;
        private System.Windows.Forms.TextBox TxtTratamiento;
        private System.Windows.Forms.TextBox TxtAROD;
        private System.Windows.Forms.TextBox TxtAROI;
        private System.Windows.Forms.TextBox TxtARDIP;
        private System.Windows.Forms.TextBox TxtRLOD;
        private System.Windows.Forms.TextBox TxtRLOI;
        private System.Windows.Forms.TextBox TxtRLDIP;
        private System.Windows.Forms.TextBox TxtRLADD;
        private System.Windows.Forms.PictureBox PbFotoPaciente;
        private System.Windows.Forms.Button BtnBuscar;
    }
}