﻿namespace Clinica.Formularios
{
    partial class GestionUsuarios
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GestionUsuarios));
            this.DgUsuarios = new System.Windows.Forms.DataGridView();
            this.BtnBuscar = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.TxtNombre = new System.Windows.Forms.TextBox();
            this.TxtUsername = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.BtnCambiarContrasena = new System.Windows.Forms.Button();
            this.BtnDeleteUser = new System.Windows.Forms.Button();
            this.BtnEditUser = new System.Windows.Forms.Button();
            this.BtnAddUser = new System.Windows.Forms.Button();
            this.TxtApellido = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.DgUsuarios)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // DgUsuarios
            // 
            this.DgUsuarios.AllowUserToAddRows = false;
            this.DgUsuarios.AllowUserToDeleteRows = false;
            this.DgUsuarios.BackgroundColor = System.Drawing.Color.White;
            this.DgUsuarios.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgUsuarios.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.DgUsuarios.Location = new System.Drawing.Point(0, 438);
            this.DgUsuarios.MaximumSize = new System.Drawing.Size(0, 1500);
            this.DgUsuarios.Name = "DgUsuarios";
            this.DgUsuarios.ReadOnly = true;
            this.DgUsuarios.Size = new System.Drawing.Size(1041, 378);
            this.DgUsuarios.TabIndex = 0;
            this.DgUsuarios.Visible = false;
            // 
            // BtnBuscar
            // 
            this.BtnBuscar.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.BtnBuscar.AutoSize = true;
            this.BtnBuscar.FlatAppearance.BorderSize = 0;
            this.BtnBuscar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnBuscar.Image = global::Clinica.Properties.Resources.icons8_encuentra_hombre_usuario_321;
            this.BtnBuscar.Location = new System.Drawing.Point(782, 200);
            this.BtnBuscar.MaximumSize = new System.Drawing.Size(93, 77);
            this.BtnBuscar.MinimumSize = new System.Drawing.Size(43, 37);
            this.BtnBuscar.Name = "BtnBuscar";
            this.BtnBuscar.Size = new System.Drawing.Size(93, 77);
            this.BtnBuscar.TabIndex = 235;
            this.BtnBuscar.UseVisualStyleBackColor = true;
            this.BtnBuscar.Click += new System.EventHandler(this.BtnBuscar_Click);
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label4.Location = new System.Drawing.Point(477, 151);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(96, 25);
            this.label4.TabIndex = 233;
            this.label4.Text = "Usuarios";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.Image = global::Clinica.Properties.Resources.Transparent_Logo;
            this.pictureBox1.Location = new System.Drawing.Point(440, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(187, 121);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 232;
            this.pictureBox1.TabStop = false;
            // 
            // TxtNombre
            // 
            this.TxtNombre.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.TxtNombre.Location = new System.Drawing.Point(373, 229);
            this.TxtNombre.Name = "TxtNombre";
            this.TxtNombre.Size = new System.Drawing.Size(393, 20);
            this.TxtNombre.TabIndex = 226;
            // 
            // TxtUsername
            // 
            this.TxtUsername.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.TxtUsername.Location = new System.Drawing.Point(373, 203);
            this.TxtUsername.Name = "TxtUsername";
            this.TxtUsername.Size = new System.Drawing.Size(393, 20);
            this.TxtUsername.TabIndex = 225;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(298, 206);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 13);
            this.label2.TabIndex = 224;
            this.label2.Text = "Usuario";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(298, 232);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 13);
            this.label1.TabIndex = 223;
            this.label1.Text = "Nombres";
            // 
            // BtnCambiarContrasena
            // 
            this.BtnCambiarContrasena.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.BtnCambiarContrasena.AutoSize = true;
            this.BtnCambiarContrasena.FlatAppearance.BorderSize = 0;
            this.BtnCambiarContrasena.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnCambiarContrasena.Image = global::Clinica.Properties.Resources.icons8_llave_32;
            this.BtnCambiarContrasena.Location = new System.Drawing.Point(654, 306);
            this.BtnCambiarContrasena.MaximumSize = new System.Drawing.Size(93, 77);
            this.BtnCambiarContrasena.MinimumSize = new System.Drawing.Size(43, 37);
            this.BtnCambiarContrasena.Name = "BtnCambiarContrasena";
            this.BtnCambiarContrasena.Size = new System.Drawing.Size(93, 77);
            this.BtnCambiarContrasena.TabIndex = 239;
            this.BtnCambiarContrasena.UseVisualStyleBackColor = true;
            this.BtnCambiarContrasena.Click += new System.EventHandler(this.BtnCambiarContrasena_Click_1);
            // 
            // BtnDeleteUser
            // 
            this.BtnDeleteUser.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.BtnDeleteUser.AutoSize = true;
            this.BtnDeleteUser.FlatAppearance.BorderSize = 0;
            this.BtnDeleteUser.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnDeleteUser.Image = global::Clinica.Properties.Resources.icons8_retire_hombre_usuario_32;
            this.BtnDeleteUser.Location = new System.Drawing.Point(555, 306);
            this.BtnDeleteUser.MaximumSize = new System.Drawing.Size(93, 77);
            this.BtnDeleteUser.MinimumSize = new System.Drawing.Size(43, 37);
            this.BtnDeleteUser.Name = "BtnDeleteUser";
            this.BtnDeleteUser.Size = new System.Drawing.Size(93, 77);
            this.BtnDeleteUser.TabIndex = 238;
            this.BtnDeleteUser.UseVisualStyleBackColor = true;
            this.BtnDeleteUser.Click += new System.EventHandler(this.BtnDeleteUser_Click);
            // 
            // BtnEditUser
            // 
            this.BtnEditUser.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.BtnEditUser.AutoSize = true;
            this.BtnEditUser.FlatAppearance.BorderSize = 0;
            this.BtnEditUser.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnEditUser.Image = global::Clinica.Properties.Resources.icons8_editar_usuario_masculino_32;
            this.BtnEditUser.Location = new System.Drawing.Point(456, 306);
            this.BtnEditUser.MaximumSize = new System.Drawing.Size(93, 77);
            this.BtnEditUser.MinimumSize = new System.Drawing.Size(43, 37);
            this.BtnEditUser.Name = "BtnEditUser";
            this.BtnEditUser.Size = new System.Drawing.Size(93, 77);
            this.BtnEditUser.TabIndex = 237;
            this.BtnEditUser.UseVisualStyleBackColor = true;
            this.BtnEditUser.Click += new System.EventHandler(this.BtnEditUser_Click_1);
            // 
            // BtnAddUser
            // 
            this.BtnAddUser.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.BtnAddUser.AutoSize = true;
            this.BtnAddUser.FlatAppearance.BorderSize = 0;
            this.BtnAddUser.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnAddUser.Image = global::Clinica.Properties.Resources.icons8_añadir_usuario_masculino_32;
            this.BtnAddUser.Location = new System.Drawing.Point(357, 306);
            this.BtnAddUser.MaximumSize = new System.Drawing.Size(93, 77);
            this.BtnAddUser.MinimumSize = new System.Drawing.Size(43, 37);
            this.BtnAddUser.Name = "BtnAddUser";
            this.BtnAddUser.Size = new System.Drawing.Size(93, 77);
            this.BtnAddUser.TabIndex = 236;
            this.BtnAddUser.UseVisualStyleBackColor = true;
            this.BtnAddUser.Click += new System.EventHandler(this.BtnAddUser_Click_1);
            // 
            // TxtApellido
            // 
            this.TxtApellido.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.TxtApellido.Location = new System.Drawing.Point(373, 257);
            this.TxtApellido.Name = "TxtApellido";
            this.TxtApellido.Size = new System.Drawing.Size(393, 20);
            this.TxtApellido.TabIndex = 241;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(298, 260);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 13);
            this.label3.TabIndex = 240;
            this.label3.Text = "Apellidos";
            // 
            // GestionUsuarios
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1041, 816);
            this.Controls.Add(this.TxtApellido);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.BtnCambiarContrasena);
            this.Controls.Add(this.BtnDeleteUser);
            this.Controls.Add(this.BtnEditUser);
            this.Controls.Add(this.BtnAddUser);
            this.Controls.Add(this.BtnBuscar);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.TxtNombre);
            this.Controls.Add(this.TxtUsername);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.DgUsuarios);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "GestionUsuarios";
            this.Text = "GestionUsuarios";
            ((System.ComponentModel.ISupportInitialize)(this.DgUsuarios)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView DgUsuarios;
        private System.Windows.Forms.Button BtnBuscar;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox TxtNombre;
        private System.Windows.Forms.TextBox TxtUsername;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button BtnCambiarContrasena;
        private System.Windows.Forms.Button BtnDeleteUser;
        private System.Windows.Forms.Button BtnEditUser;
        private System.Windows.Forms.Button BtnAddUser;
        private System.Windows.Forms.TextBox TxtApellido;
        private System.Windows.Forms.Label label3;
    }
}