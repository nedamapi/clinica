﻿namespace Clinica.Formularios.CRUD_Paciente
{
    partial class VerTodos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VerTodos));
            this.DgPacientes = new System.Windows.Forms.DataGridView();
            this.label4 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.BtnAddPaciente = new System.Windows.Forms.Button();
            this.BtnEditPaciente = new System.Windows.Forms.Button();
            this.BtnDeletePaciente = new System.Windows.Forms.Button();
            this.BtnBuscar = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtNombre = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtCedula = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.PbFotoPaciente = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.DgPacientes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PbFotoPaciente)).BeginInit();
            this.SuspendLayout();
            // 
            // DgPacientes
            // 
            this.DgPacientes.AllowUserToAddRows = false;
            this.DgPacientes.AllowUserToDeleteRows = false;
            this.DgPacientes.BackgroundColor = System.Drawing.Color.White;
            this.DgPacientes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgPacientes.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.DgPacientes.Location = new System.Drawing.Point(0, 539);
            this.DgPacientes.Margin = new System.Windows.Forms.Padding(4);
            this.DgPacientes.MaximumSize = new System.Drawing.Size(0, 1600);
            this.DgPacientes.Name = "DgPacientes";
            this.DgPacientes.ReadOnly = true;
            this.DgPacientes.RowHeadersWidth = 51;
            this.DgPacientes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DgPacientes.Size = new System.Drawing.Size(1388, 465);
            this.DgPacientes.TabIndex = 9;
            this.DgPacientes.Visible = false;
            this.DgPacientes.SelectionChanged += new System.EventHandler(this.DgPacientes_SelectionChanged);
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label4.Location = new System.Drawing.Point(643, 186);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(143, 31);
            this.label4.TabIndex = 21;
            this.label4.Text = "Pacientes";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.Image = global::Clinica.Properties.Resources.Transparent_Logo;
            this.pictureBox1.Location = new System.Drawing.Point(593, 15);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(249, 149);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 20;
            this.pictureBox1.TabStop = false;
            // 
            // BtnAddPaciente
            // 
            this.BtnAddPaciente.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.BtnAddPaciente.AutoSize = true;
            this.BtnAddPaciente.FlatAppearance.BorderSize = 0;
            this.BtnAddPaciente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnAddPaciente.Image = global::Clinica.Properties.Resources.icons8_añadir_usuario_masculino_32;
            this.BtnAddPaciente.Location = new System.Drawing.Point(533, 366);
            this.BtnAddPaciente.Margin = new System.Windows.Forms.Padding(4);
            this.BtnAddPaciente.MaximumSize = new System.Drawing.Size(124, 95);
            this.BtnAddPaciente.MinimumSize = new System.Drawing.Size(57, 46);
            this.BtnAddPaciente.Name = "BtnAddPaciente";
            this.BtnAddPaciente.Size = new System.Drawing.Size(124, 95);
            this.BtnAddPaciente.TabIndex = 15;
            this.BtnAddPaciente.UseVisualStyleBackColor = true;
            this.BtnAddPaciente.Click += new System.EventHandler(this.BtnAddPaciente_Click);
            // 
            // BtnEditPaciente
            // 
            this.BtnEditPaciente.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.BtnEditPaciente.AutoSize = true;
            this.BtnEditPaciente.FlatAppearance.BorderSize = 0;
            this.BtnEditPaciente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnEditPaciente.Image = global::Clinica.Properties.Resources.icons8_editar_usuario_masculino_32;
            this.BtnEditPaciente.Location = new System.Drawing.Point(665, 366);
            this.BtnEditPaciente.Margin = new System.Windows.Forms.Padding(4);
            this.BtnEditPaciente.MaximumSize = new System.Drawing.Size(124, 95);
            this.BtnEditPaciente.MinimumSize = new System.Drawing.Size(57, 46);
            this.BtnEditPaciente.Name = "BtnEditPaciente";
            this.BtnEditPaciente.Size = new System.Drawing.Size(124, 95);
            this.BtnEditPaciente.TabIndex = 16;
            this.BtnEditPaciente.UseVisualStyleBackColor = true;
            this.BtnEditPaciente.Click += new System.EventHandler(this.BtnEditPaciente_Click);
            // 
            // BtnDeletePaciente
            // 
            this.BtnDeletePaciente.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.BtnDeletePaciente.AutoSize = true;
            this.BtnDeletePaciente.FlatAppearance.BorderSize = 0;
            this.BtnDeletePaciente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnDeletePaciente.Image = global::Clinica.Properties.Resources.icons8_retire_hombre_usuario_32;
            this.BtnDeletePaciente.Location = new System.Drawing.Point(797, 366);
            this.BtnDeletePaciente.Margin = new System.Windows.Forms.Padding(4);
            this.BtnDeletePaciente.MaximumSize = new System.Drawing.Size(124, 95);
            this.BtnDeletePaciente.MinimumSize = new System.Drawing.Size(57, 46);
            this.BtnDeletePaciente.Name = "BtnDeletePaciente";
            this.BtnDeletePaciente.Size = new System.Drawing.Size(124, 95);
            this.BtnDeletePaciente.TabIndex = 17;
            this.BtnDeletePaciente.UseVisualStyleBackColor = true;
            this.BtnDeletePaciente.Click += new System.EventHandler(this.BtnDeletePaciente_Click);
            // 
            // BtnBuscar
            // 
            this.BtnBuscar.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.BtnBuscar.AutoSize = true;
            this.BtnBuscar.FlatAppearance.BorderSize = 0;
            this.BtnBuscar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnBuscar.Image = global::Clinica.Properties.Resources.icons8_encuentra_hombre_usuario_321;
            this.BtnBuscar.Location = new System.Drawing.Point(1049, 246);
            this.BtnBuscar.Margin = new System.Windows.Forms.Padding(4);
            this.BtnBuscar.MaximumSize = new System.Drawing.Size(124, 95);
            this.BtnBuscar.MinimumSize = new System.Drawing.Size(57, 46);
            this.BtnBuscar.Name = "BtnBuscar";
            this.BtnBuscar.Size = new System.Drawing.Size(124, 95);
            this.BtnBuscar.TabIndex = 222;
            this.BtnBuscar.UseVisualStyleBackColor = true;
            this.BtnBuscar.Click += new System.EventHandler(this.BtnBuscar_Click);
            // 
            // textBox1
            // 
            this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.textBox1.Location = new System.Drawing.Point(509, 246);
            this.textBox1.Margin = new System.Windows.Forms.Padding(4);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(523, 22);
            this.textBox1.TabIndex = 227;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(309, 253);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(130, 17);
            this.label1.TabIndex = 226;
            this.label1.Text = "Codigo de paciente";
            // 
            // TxtNombre
            // 
            this.TxtNombre.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.TxtNombre.Location = new System.Drawing.Point(509, 282);
            this.TxtNombre.Margin = new System.Windows.Forms.Padding(4);
            this.TxtNombre.Name = "TxtNombre";
            this.TxtNombre.Size = new System.Drawing.Size(523, 22);
            this.TxtNombre.TabIndex = 229;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(309, 286);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(140, 17);
            this.label2.TabIndex = 228;
            this.label2.Text = "Nombre del Paciente";
            // 
            // TxtCedula
            // 
            this.TxtCedula.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.TxtCedula.Location = new System.Drawing.Point(509, 319);
            this.TxtCedula.Margin = new System.Windows.Forms.Padding(4);
            this.TxtCedula.Name = "TxtCedula";
            this.TxtCedula.Size = new System.Drawing.Size(523, 22);
            this.TxtCedula.TabIndex = 233;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(309, 323);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(133, 17);
            this.label3.TabIndex = 232;
            this.label3.Text = "Cedula del paciente";
            // 
            // PbFotoPaciente
            // 
            this.PbFotoPaciente.Location = new System.Drawing.Point(312, 107);
            this.PbFotoPaciente.Name = "PbFotoPaciente";
            this.PbFotoPaciente.Size = new System.Drawing.Size(125, 110);
            this.PbFotoPaciente.TabIndex = 234;
            this.PbFotoPaciente.TabStop = false;
            // 
            // VerTodos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1388, 1004);
            this.Controls.Add(this.PbFotoPaciente);
            this.Controls.Add(this.TxtCedula);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.TxtNombre);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.BtnBuscar);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.BtnDeletePaciente);
            this.Controls.Add(this.BtnEditPaciente);
            this.Controls.Add(this.BtnAddPaciente);
            this.Controls.Add(this.DgPacientes);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "VerTodos";
            this.Text = "VerTodos";
            this.Load += new System.EventHandler(this.VerTodos_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DgPacientes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PbFotoPaciente)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        public System.Windows.Forms.DataGridView DgPacientes;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button BtnAddPaciente;
        private System.Windows.Forms.Button BtnEditPaciente;
        private System.Windows.Forms.Button BtnDeletePaciente;
        private System.Windows.Forms.Button BtnBuscar;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TxtNombre;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TxtCedula;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox PbFotoPaciente;
    }
}