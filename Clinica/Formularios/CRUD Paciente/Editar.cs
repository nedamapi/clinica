﻿using Clinica.Clases;
using Clinica.Datos.BBDD;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Clinica.Formularios.CRUD_Paciente
{
    public partial class Editar : Form
    {
        VerTodos _todos = new VerTodos();
        string filePath = string.Empty;

        public Editar(VerTodos todos)
        {
            InitializeComponent();
            _todos = todos;
        }
        OftalmologiaEntities DB = new OftalmologiaEntities();
        private void Limpiar()
        {
            TxtNombre.Clear();
            TxtCedula.Clear();
            TxtOficio.Clear();
            MTxtTelefono.Clear();
        }
        private void Editar_Load(object sender, EventArgs e)
        {
            try
            {
                var sexos = DB.Sexos.ToList();

                if(sexos != null)
                {
                    CmbSexo.DataSource = sexos;
                    CmbSexo.ValueMember = "IdSexo";
                    CmbSexo.DisplayMember = "Descripcion";
                }
                else
                {
                    MessageBox.Show("La tabla de sexos está vacía. Favor comunicarse con el administrador.", "Sistema Fundación Optimedica ",MessageBoxButtons.OK,MessageBoxIcon.Error);
                    return;
                }

                var paciente = DB.Pacientes.SingleOrDefault(x => x.CodigoPaciente == PropidadesVariables.CurrentPaciente);

                if(paciente != null)
                {
                    TxtNombre.Text = paciente.Nombre;
                    TxtCedula.Text = paciente.Cedula;
                    TxtOficio.Text = paciente.Oficio;
                    MTxtTelefono.Text = paciente.Telefono;
                    DtpFechaNacimiento.Value = paciente.FechaNacimiento.Value;
                    CmbSexo.SelectedValue = paciente.IdSexo;
                    TxtEdad.Text = paciente.Edad.ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Ha sucedido un error. \n {ex.Message}");
            }
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void BtnCerrar_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void BtnIngresar_Click(object sender, EventArgs e)
        {
            try
            {
                var paciente = DB.Pacientes.SingleOrDefault(x => x.CodigoPaciente == PropidadesVariables.CurrentPaciente);
                
                if(paciente != null)
                {
                    string telefono = MTxtTelefono.Text.Replace("(", "").Replace(")", "").Replace("-", "").Replace(" ", "");

                    paciente.Nombre = TxtNombre.Text;
                    paciente.Cedula = TxtCedula.Text;
                    paciente.FechaNacimiento = DtpFechaNacimiento.Value;
                    paciente.Edad = Convert.ToInt32(TxtEdad.Text);
                    paciente.Telefono = telefono;
                    paciente.IdSexo = Convert.ToInt32(CmbSexo.SelectedValue.ToString());
                    paciente.Oficio = TxtOficio.Text;
                    paciente.IdUsuarioModifica = PropidadesVariables.CurrentUserID;
                    paciente.FechaModificacion = DateTime.Now;

                    DB.SaveChanges();

                    if(!String.IsNullOrWhiteSpace(filePath))
                    {
                        var foto = DB.FotoPaciente.SingleOrDefault(x => x.CodigoPaciente == PropidadesVariables.CurrentPaciente);
                        foto.RutaImagen = filePath;
                        DB.SaveChanges();
                    }

                    Limpiar();
                    _todos.DgPacientes.DataSource = (from p in DB.Pacientes
                                                     join s in DB.Sexos
                                                     on p.IdSexo equals s.IdSexo
                                                     where p.Estado == true
                                                     select new
                                                     {
                                                         p.CodigoPaciente,
                                                         p.Nombre,
                                                         p.Cedula,
                                                         p.FechaNacimiento,
                                                         p.Edad,
                                                         s.Descripcion,
                                                         p.Telefono,
                                                         p.Oficio
                                                     }).ToList(); 
                    this.Hide();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Ha sucedido un error. \n {ex.Message}");
            }
        }

        private void BtnMinimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.InitialDirectory = "c:\\";
                openFileDialog.Filter = "All files (*.*)|*.*| Image file (*.jpg, *.jpeg, *.png)|*.jpg; *.jpeg; *.png";
                openFileDialog.FilterIndex = 2;
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    filePath = openFileDialog.FileName;
                }
            }
        }
    }
}
