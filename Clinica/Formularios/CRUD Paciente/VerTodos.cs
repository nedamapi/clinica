﻿using Clinica.Clases;
using Clinica.Datos.BBDD;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Clinica.Formularios.CRUD_Paciente
{
    public partial class VerTodos : Form
    {
        public VerTodos()
        {
            InitializeComponent();
        }
        public OftalmologiaEntities DB = new OftalmologiaEntities();
        string code = string.Empty;

        private void VerTodos_Load(object sender, EventArgs e)
        {
            if(PropidadesVariables.CurrentUserRoleID != 1)
            {
                BtnDeletePaciente.Visible = false;
                BtnAddPaciente.Location = new Point(450, 297);
                BtnEditPaciente.Location = new Point(549, 297);
            }
        }

        private void BtnAddPaciente_Click(object sender, EventArgs e)
        {
            Nuevo addPaciente = new Nuevo(this);
            addPaciente.Show();
        }

        private void BtnEditPaciente_Click(object sender, EventArgs e)
        {
            if (!DgPacientes.Visible)
            {
                MessageBox.Show("No se ha seleccionado ningún paciente para editar.", "Sistema Fundación Optimedica ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                PropidadesVariables.CurrentPaciente = DgPacientes.CurrentRow.Cells[0].Value.ToString();
                Editar editPaciente = new Editar(this);
                editPaciente.Show();
            }
        }

        private void BtnDeletePaciente_Click(object sender, EventArgs e)
        {
            if(!DgPacientes.Visible)
            {
                MessageBox.Show("No se ha seleccionado ningún paciente para borrar.", "Sistema Fundación Optimedica ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                DialogResult result = MessageBox.Show("¿Está seguro de eliminar este paciente?", "Confirmación", MessageBoxButtons.YesNo);
                if (result == DialogResult.Yes)
                {
                    try
                    {
                        PropidadesVariables.CurrentPaciente = DgPacientes.CurrentRow.Cells[1].Value.ToString();

                        var paciente = DB.Pacientes.SingleOrDefault(x => x.CodigoPaciente == PropidadesVariables.CurrentPaciente);

                        if (paciente != null)
                        {
                            paciente.Estado = false;

                            DB.SaveChanges();
                            DgPacientes.DataSource = (from p in DB.Pacientes
                                                      join s in DB.Sexos
                                                      on p.IdSexo equals s.IdSexo
                                                      where p.Estado == true
                                                      select new
                                                      {
                                                          p.ID,
                                                          p.CodigoPaciente,
                                                          p.Nombre,
                                                          p.Cedula,
                                                          p.FechaNacimiento,
                                                          p.Edad,
                                                          s.Descripcion,
                                                          p.Telefono,
                                                          p.Oficio
                                                      }).ToList();

                            MessageBox.Show("\nPaciente eliminado correctamente.\n", "Sistema Fundación Optimedica ", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show($"Ha sucedido un error. \n {ex.Message}");
                    }
                }
            }
        }

        private void BtnBuscar_Click(object sender, EventArgs e)
        {
            var pacientes = new object();

            if(!String.IsNullOrWhiteSpace(textBox1.Text))
            {
                pacientes = (from p in DB.Pacientes
                             join s in DB.Sexos
                             on p.IdSexo equals s.IdSexo
                             where p.Estado == true && p.CodigoPaciente.Contains(textBox1.Text)
                             select new
                             {
                                 p.CodigoPaciente,
                                 p.Nombre,
                                 p.Cedula,
                                 p.FechaNacimiento,
                                 p.Edad,
                                 s.Descripcion,
                                 p.Telefono,
                                 p.Oficio
                             }).ToList();
            }
            else if(!String.IsNullOrWhiteSpace(TxtNombre.Text))
            {
                pacientes = (from p in DB.Pacientes
                             join s in DB.Sexos
                             on p.IdSexo equals s.IdSexo
                             where p.Estado == true && p.Nombre.Contains(TxtNombre.Text)
                             select new
                             {
                                 p.CodigoPaciente,
                                 p.Nombre,
                                 p.Cedula,
                                 p.FechaNacimiento,
                                 p.Edad,
                                 s.Descripcion,
                                 p.Telefono,
                                 p.Oficio
                             }).ToList();
            }
            else if(!String.IsNullOrWhiteSpace(TxtCedula.Text))
            {
                pacientes = (from p in DB.Pacientes
                             join s in DB.Sexos
                             on p.IdSexo equals s.IdSexo
                             where 
                                p.Estado == true 
                                && p.Cedula.Contains(TxtCedula.Text)
                             select new
                             {
                                 p.CodigoPaciente,
                                 p.Nombre,
                                 p.Cedula,
                                 p.FechaNacimiento,
                                 p.Edad,
                                 s.Descripcion,
                                 p.Telefono,
                                 p.Oficio
                             }).ToList();
            }
            else
            {
                pacientes = (from p in DB.Pacientes
                             join s in DB.Sexos
                             on p.IdSexo equals s.IdSexo
                             select new
                             {
                                 p.CodigoPaciente,
                                 p.Nombre,
                                 p.Cedula,
                                 p.FechaNacimiento,
                                 p.Edad,
                                 s.Descripcion,
                                 p.Telefono,
                                 p.Oficio
                             }).ToList();
            }

            if (pacientes != null)
            {
                DgPacientes.DataSource = pacientes;
                DgPacientes.Columns[2].Width = 250;
                DgPacientes.Columns[6].HeaderText = "Sexo";
                DgPacientes.Visible = true;
            }
        }
        
        private void DgPacientes_SelectionChanged(object sender, EventArgs e)
        {
            Logica logica = new Logica();
            code = DgPacientes.CurrentRow.Cells[0].Value.ToString();
            logica.FillImage(code, PbFotoPaciente);
        }
    }
}
