﻿using Clinica.Clases;
using Clinica.Datos.BBDD;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Clinica.Formularios.CRUD_Paciente
{
    public partial class Nuevo : Form
    {
        VerTodos _todos = new VerTodos();
        string filePath = string.Empty;

        public Nuevo(VerTodos todos)
        {
            InitializeComponent();
            _todos = todos;
        }

        OftalmologiaEntities DB = new OftalmologiaEntities();

        private void Nuevo_Load(object sender, EventArgs e)
        {
            var sexos = DB.Sexos.ToList();

            CmbSexo.DataSource = sexos;
            CmbSexo.ValueMember = "IdSexo";
            CmbSexo.DisplayMember = "Descripcion";
        }

        private void BtnMinimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void BtnCerrar_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
        private void Limpiar()
        {
            TxtNombre.Clear();
            TxtCedula.Clear();
            TxtOficio.Clear();
            MTxtTelefono.Clear();
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void BtnIngresar_Click_1(object sender, EventArgs e)
        {
            try
            {
                DateTime zeroTime = new DateTime(1, 1, 1);
                DateTime a = DtpFechaNacimiento.Value;
                DateTime b = DateTime.Now;
                TimeSpan span = b - a;
                int edad = (zeroTime + span).Year - 1;
                string letraNombre = TxtApellido.Text.Substring(0, 1);
                string codigo = string.Empty;
                var ultimoRegistroLetra = DB.Pacientes.Where(x => x.CodigoPaciente.Contains(letraNombre)).OrderByDescending(x => x.ID).Take(1);
                if (ultimoRegistroLetra.Count() > 0)
                {
                    int numero = Convert.ToInt32(ultimoRegistroLetra.SingleOrDefault().CodigoPaciente.Replace(TxtApellido.Text.Substring(0, 1).ToUpper().ToString(), ""));
                    codigo = TxtApellido.Text.Substring(0, 1) + (numero + 1);
                }
                else
                {
                    codigo = TxtApellido.Text.Substring(0, 1) + 1;
                }
                string telefono = MTxtTelefono.Text.Replace("(", "").Replace(")", "").Replace("-", "").Replace(" ", "");
                if(DtpFechaNacimiento.Value.ToShortDateString() == DateTime.Now.ToShortDateString())
                {
                    MessageBox.Show("La fecha de nacimiento no se ha seleccionado. Favor verificar.", "Sistema Fundación Optimedica", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                DB.Pacientes.Add(new Pacientes
                {
                    ID = 0,
                    Nombre = TxtNombre.Text + " " + TxtApellido.Text,
                    CodigoPaciente = codigo.ToUpper(),
                    Cedula = TxtCedula.Text,
                    FechaNacimiento = DtpFechaNacimiento.Value,
                    Edad = edad,
                    IdSexo = Convert.ToInt32(CmbSexo.SelectedValue.ToString()),
                    Telefono = telefono,
                    Oficio = TxtOficio.Text,
                    IdUsuarioCreador = PropidadesVariables.CurrentUserID,
                    FechaCreacion = DateTime.Now,
                    Estado = true
                });
                DB.SaveChanges();

                if(!String.IsNullOrWhiteSpace(filePath))
                {
                    DB.FotoPaciente.Add(new FotoPaciente
                    {
                        CodigoPaciente = codigo,
                        RutaImagen = filePath
                    });
                    DB.SaveChanges();
                }

                Limpiar();
                _todos.DgPacientes.DataSource = (from p in DB.Pacientes
                                                 join s in DB.Sexos
                                                 on p.IdSexo equals s.IdSexo
                                                 where p.Estado == true
                                                 select new
                                                 {
                                                     p.ID,
                                                     p.CodigoPaciente,
                                                     p.Nombre,
                                                     p.Cedula,
                                                     p.FechaNacimiento,
                                                     p.Edad,
                                                     s.Descripcion,
                                                     p.Telefono,
                                                     p.Oficio
                                                 }).ToList();
                MessageBox.Show($"El codigo de paciente es: {codigo}", "Sistema Fundación Optimedica", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Hide();
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Ha sucedido un error. \n {ex.Message}");
            }
        }

        private void BtnCancelar_Click_1(object sender, EventArgs e)
        {
            Limpiar();
            this.Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.InitialDirectory = "c:\\";
                openFileDialog.Filter = "All files (*.*)|*.*| Image file (*.jpg, *.jpeg, *.png)|*.jpg; *.jpeg; *.png";
                openFileDialog.FilterIndex = 2;
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    filePath = openFileDialog.FileName;
                }
            }
        }
    }
}
