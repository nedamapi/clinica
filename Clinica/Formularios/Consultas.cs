﻿using Clinica.Clases;
using Clinica.Datos.BBDD;
using Clinica.Formularios.HistorialConsultas;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Clinica.Formularios
{
    public partial class Consultas : Form
    {
        Principal Principal = new Principal();
        public Consultas()
        {
            InitializeComponent();
        }
        public OftalmologiaEntities DB = new OftalmologiaEntities();
        bool isPrimeraConsulta = false;

        private void Consultas_Load(object sender, EventArgs e)
        {
            BtnGuardar.Enabled = false;
            BtnCancelar.Enabled = false;
        }
        public void Limpiar()
        {
            TxtUltimaConsulta.Clear();
            TxtAntecedentes.Clear();
            TxtLAOD.Clear();
            TxtLAOI.Clear();
            TxtLADIP.Clear();
            TxtLAADD.Clear();
            TxtInspeccionFisica.Clear();
            TxtParpados.Clear();
            TxtConjuntivas.Clear();
            TxtCorneas.Clear();
            TxtIris.Clear();
            TxtPupilas.Clear();
            TxtSCOD.Clear();
            TxtSCOI.Clear();
            TxtSCJ.Clear();
            TxtSSOD.Clear();
            TxtSSOI.Clear();
            TxtSSJ.Clear();
            TxtPIOI.Clear();
            TxtPIOD.Clear();
            TxtRLOD.Clear();
            TxtRLOI.Clear();
            TxtRLDIP.Clear();
            TxtRLADD.Clear();
            TxtFondoscopia.Clear();
            TxtBiomicrospia.Clear();
            TxtDiagnostico.Clear();
            TxtPlan.Clear();
            TxtTratamiento.Clear();
            TxtAROD.Clear();
            TxtAROI.Clear();
            TxtARDIP.Clear();
            dtpFechaConsulta.Value = DateTime.Now;
        }
        public void LimpiarPacial()
        {
            TxtUltimaConsulta.Clear() ;
            TxtAntecedentes.Clear() ;
            TxtInspeccionFisica.Clear() ;
            TxtParpados.Clear() ;
            TxtConjuntivas.Clear() ;
            TxtCorneas.Clear() ;
            TxtIris.Clear() ;
            TxtPupilas.Clear() ;
            TxtSCOD.Clear() ;
            TxtSCOI.Clear() ;
            TxtSCJ.Clear() ;
            TxtSSOD.Clear() ;
            TxtSSOI.Clear() ;
            TxtSSJ.Clear() ;
            TxtPIOI.Clear();
            TxtPIOD.Clear() ;
            TxtFondoscopia.Clear() ;
            TxtBiomicrospia.Clear() ;
            TxtDiagnostico.Clear() ;
            TxtPlan.Clear() ;
            TxtTratamiento.Clear() ;
            TxtAROD.Clear() ;
            TxtAROI.Clear() ;
            TxtARDIP.Clear() ;
            dtpFechaConsulta.Value = DateTime.Now;
        }
        public void Campos(bool response)
        {
            TxtUltimaConsulta.ReadOnly = response;
            TxtAntecedentes.ReadOnly = response;
            TxtInspeccionFisica.ReadOnly = response;
            TxtParpados.ReadOnly = response;
            TxtConjuntivas.ReadOnly = response;
            TxtCorneas.ReadOnly = response;
            TxtIris.ReadOnly = response;
            TxtPupilas.ReadOnly = response;
            TxtSCOD.ReadOnly = response;
            TxtSCOI.ReadOnly = response;
            TxtSCJ.ReadOnly = response;
            TxtSSOD.ReadOnly = response;
            TxtSSOI.ReadOnly = response;
            TxtSSJ.ReadOnly = response;
            TxtPIOI.ReadOnly = response;
            TxtPIOD.ReadOnly = response;
            TxtFondoscopia.ReadOnly = response;
            TxtBiomicrospia.ReadOnly = response;
            TxtDiagnostico.ReadOnly = response;
            TxtPlan.ReadOnly = response;
            TxtTratamiento.ReadOnly = response;
            TxtAROD.ReadOnly = response;
            TxtAROI.ReadOnly = response;
            TxtARDIP.ReadOnly = response;
            TxtRLOD.ReadOnly = response;
            TxtRLOI.ReadOnly = response;
            TxtRLADD.ReadOnly = response;
            TxtRLDIP.ReadOnly = response;
        }
        public void Botones(bool response)
        {
            BtnGuardar.Enabled = response;
            BtnCancelar.Enabled = response;
        }

        private void nuevaConsultaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(!String.IsNullOrWhiteSpace(TxtCodigo.Text))
            {
                Campos(false);
                TxtCodigo.ReadOnly = true;
                Botones(true);
                LimpiarPacial();
            }
            else
            {
                MessageBox.Show("No se ha escrito el codigo de paciente. Favor verificar.", "Sistema Fundación Optimedica", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            if (isPrimeraConsulta)
            {
                TxtLAOD.ReadOnly = false;
                TxtLAOI.ReadOnly = false;
                TxtLAADD.ReadOnly = false;
                TxtLADIP.ReadOnly = false;
            }
        }

        private void historialToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(TxtCodigo.Text != null && !String.IsNullOrWhiteSpace(TxtCodigo.Text))
            {
                PropidadesVariables.CurrentPacienteHistorial = TxtCodigo.Text;
                HistorialConsulta historial = new HistorialConsulta();
                historial.Show();
            }
            else
            {
                MessageBox.Show("El campo codigo de paciente no puede estar vacío, favor verificar.", "Sistema Fundación Optimedica", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void BtnCancelar_Click_1(object sender, EventArgs e)
        {
            Campos(true);
            TxtCodigo.ReadOnly = false;
            Botones(false);
            Limpiar();
            if(!TxtLAOD.ReadOnly)
            {
                TxtLAOD.ReadOnly = true;
                TxtLAOI.ReadOnly = true;
                TxtLAADD.ReadOnly = true;
                TxtLADIP.ReadOnly = true;
            }
        }

        private void BtnGuardar_Click_1(object sender, EventArgs e)
        {
            Clases.Consultas consultas = new Clases.Consultas();
            try
            {                    
                if(!TxtLAOD.ReadOnly && !String.IsNullOrWhiteSpace(TxtLAOD.Text))
                    consultas.InsertConsulta(TxtCodigo.Text, TxtUltimaConsulta.Text, TxtUltimaConsulta.Text, TxtLAOD.Text, TxtLAOI.Text, TxtLADIP.Text, TxtLAADD.Text,
                                             TxtInspeccionFisica.Text, TxtParpados.Text, TxtConjuntivas.Text, TxtCorneas.Text, TxtIris.Text, TxtPupilas.Text, TxtSCOD.Text,
                                             TxtSCOI.Text, TxtSCJ.Text, TxtSSOD.Text, TxtSSOI.Text, TxtSSJ.Text, TxtPIOI.Text, TxtPIOD.Text, TxtFondoscopia.Text, TxtBiomicrospia.Text,
                                             TxtDiagnostico.Text, TxtPlan.Text, TxtTratamiento.Text, TxtAROD.Text, TxtAROI.Text, TxtARDIP.Text, dtpFechaConsulta.Value);
                else
                    consultas.InsertConsulta(TxtCodigo.Text, TxtUltimaConsulta.Text, TxtUltimaConsulta.Text, TxtRLOD.Text, TxtRLOI.Text, TxtRLDIP.Text, TxtRLADD.Text,
                                                 TxtInspeccionFisica.Text, TxtParpados.Text, TxtConjuntivas.Text, TxtCorneas.Text, TxtIris.Text, TxtPupilas.Text, TxtSCOD.Text,
                                                 TxtSCOI.Text, TxtSCJ.Text, TxtSSOD.Text, TxtSSOI.Text, TxtSSJ.Text, TxtPIOI.Text, TxtPIOD.Text, TxtFondoscopia.Text, TxtBiomicrospia.Text,
                                                 TxtDiagnostico.Text, TxtPlan.Text, TxtTratamiento.Text, TxtAROD.Text, TxtAROI.Text, TxtARDIP.Text, dtpFechaConsulta.Value);
                
                if (!String.IsNullOrWhiteSpace(consultas.excepcion))
                    MessageBox.Show(consultas.excepcion, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);

                if (!TxtLAOD.ReadOnly)
                {
                    TxtLAOD.ReadOnly = true;
                    TxtLAOI.ReadOnly = true;
                    TxtLAADD.ReadOnly = true;
                    TxtLADIP.ReadOnly = true;
                }
                TxtCodigo.ReadOnly = false;
                TxtCodigo.Clear();
                Campos(true);
                Botones(true);
                Limpiar();
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Ha sucedido un error. \n {ex.Message}");
            }
        }

        private void BtnBuscar_Click(object sender, EventArgs e)
        {
            Limpiar();
            Campos(true);
            var paciente = DB.Pacientes.FirstOrDefault(x => x.CodigoPaciente == TxtCodigo.Text);

            if (paciente != null)
            {
                isPrimeraConsulta = false;
                TxtNombre.Text = paciente.Nombre;
                TxtEdad.Text = Convert.ToString(paciente.Edad);
                if (paciente.IdSexo == 1)
                {
                    TxtSexo.Text = "Masculino";
                }
                else
                {
                    TxtSexo.Text = "Femenino";
                }
                var ultimaConsulta = DB.Consultas.Where(x => x.CodigoPaciente == TxtCodigo.Text).ToList().LastOrDefault();

                if (ultimaConsulta != null)
                {
                    TxtUltimaConsulta.Text = ultimaConsulta.MotivoConsulta;
                    TxtAntecedentes.Text = ultimaConsulta.Antecedentes;
                    TxtLAOD.Text = ultimaConsulta.LAOD;
                    TxtLAOI.Text = ultimaConsulta.LAOI;
                    TxtLADIP.Text = ultimaConsulta.LADIP;
                    TxtLAADD.Text = ultimaConsulta.LAADD;
                    TxtInspeccionFisica.Text = ultimaConsulta.InspeccionFisica;
                    TxtParpados.Text = ultimaConsulta.Parpados;
                    TxtConjuntivas.Text = ultimaConsulta.Conjuntivas;
                    TxtCorneas.Text = ultimaConsulta.Corneas;
                    TxtIris.Text = ultimaConsulta.Iris;
                    TxtPupilas.Text = ultimaConsulta.Pupilas;
                    TxtSCOD.Text = ultimaConsulta.SCOD;
                    TxtSCOI.Text = ultimaConsulta.SCOI;
                    TxtSCJ.Text = ultimaConsulta.SCJ;
                    TxtSSOD.Text = ultimaConsulta.SSOD;
                    TxtSSOI.Text = ultimaConsulta.SSOI;
                    TxtSSJ.Text = ultimaConsulta.SSJ;
                    TxtPIOI.Text = ultimaConsulta.PIOI;
                    TxtPIOD.Text = ultimaConsulta.PIOD;
                    TxtFondoscopia.Text = ultimaConsulta.Fondoscopia;
                    TxtBiomicrospia.Text = ultimaConsulta.Biomicrospia;
                    TxtDiagnostico.Text = ultimaConsulta.Diagnostico;
                    TxtPlan.Text = ultimaConsulta.Plann;
                    TxtTratamiento.Text = ultimaConsulta.Tratamiento;
                    TxtAROD.Text = ultimaConsulta.AROD;
                    TxtAROI.Text = ultimaConsulta.AROI;
                    TxtARDIP.Text = ultimaConsulta.ARDIP;
                    if (ultimaConsulta.FechaConsulta == null)
                    {
                        dtpFechaConsulta.Value = DateTime.Now;
                    }
                    else
                    {
                        dtpFechaConsulta.Value = ultimaConsulta.FechaConsulta.Value;
                    }
                }
                else
                {
                    isPrimeraConsulta = true;
                    MessageBox.Show("Primera consulta para este paciente.", "Sistema Fundación Optimedica", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                Logica logica = new Logica();
                logica.FillImage(TxtCodigo.Text, PbFotoPaciente);
            }
            else
            {
                MessageBox.Show("Paciente no encontrado.", "Sistema Fundación Optimedica", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
