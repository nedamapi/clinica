//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Clinica.Datos.BBDD
{
    using System;
    using System.Collections.Generic;
    
    public partial class Consultas
    {
        public int ID { get; set; }
        public string CodigoPaciente { get; set; }
        public string MotivoConsulta { get; set; }
        public string Antecedentes { get; set; }
        public string LAOD { get; set; }
        public string LAOI { get; set; }
        public string LADIP { get; set; }
        public string LAADD { get; set; }
        public string InspeccionFisica { get; set; }
        public string Parpados { get; set; }
        public string Conjuntivas { get; set; }
        public string Corneas { get; set; }
        public string Iris { get; set; }
        public string Pupilas { get; set; }
        public string SCOD { get; set; }
        public string SCOI { get; set; }
        public string SCJ { get; set; }
        public string SSOD { get; set; }
        public string SSOI { get; set; }
        public string SSJ { get; set; }
        public string PIOD { get; set; }
        public string PIOI { get; set; }
        public string AROD { get; set; }
        public string AROI { get; set; }
        public string ARDIP { get; set; }
        public string Fondoscopia { get; set; }
        public string Biomicrospia { get; set; }
        public string Diagnostico { get; set; }
        public string Plann { get; set; }
        public string Tratamiento { get; set; }
        public Nullable<System.DateTime> FechaConsulta { get; set; }
    }
}
