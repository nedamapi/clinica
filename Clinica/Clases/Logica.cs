﻿using Clinica.Datos.BBDD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Clinica.Clases
{
    public class Logica
    {
        OftalmologiaEntities DB = new OftalmologiaEntities();

        public void FillImage(string codigoPaciente, PictureBox pictureBox)
        {
            var foto = DB.FotoPaciente.SingleOrDefault(x => x.CodigoPaciente == codigoPaciente);

            if(foto != null)
            {
                if (!String.IsNullOrEmpty(foto.RutaImagen.ToString()))
                {
                    pictureBox.ImageLocation = foto.RutaImagen.ToString();
                    pictureBox.SizeMode = PictureBoxSizeMode.StretchImage;
                }
                else
                {
                    string rutaDefault = AppDomain.CurrentDomain.BaseDirectory + "PacienteDefaul.png";
                    pictureBox.ImageLocation = rutaDefault;
                    pictureBox.SizeMode = PictureBoxSizeMode.StretchImage;
                }
            }
        }
    }
}
