﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Clinica.Clases
{
    public class PropidadesVariables
    {
        public static int CurrentUserID { get; set; }
        public static int? CurrentUserRoleID { get; set; }
        public static string CurrentUserName { get; set; }
        public static string CurrentPaciente { get; set; }
        public static string CurrentPacienteHistorial { get; set; }
        public static int ConsultaIDDetalle { get; set; }
        public static string DeleteUser { get; set; }
        public static string EditUser { get; set; }
        public static string ChangePasswordUsername { get; set; }


    }
}
