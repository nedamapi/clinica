﻿using Clinica.Datos.BBDD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Clinica.Clases
{
    public class Consultas
    {
        OftalmologiaEntities DB = new OftalmologiaEntities();
        public string excepcion { get; set; }
        public bool InsertConsulta(string codigo, string ultimaConsulta, string antecedentes, string laod, string laOi, string laDIP, string laAdd, string inspeccion,
                                    string parpados, string conjuntivas, string corneas, string iris, string pupilas,string scOd, string scOi, string scJ, string ssOd, string ssOi,
                                    string ssJ, string pioi, string piod, string fondoscopia, string biomicrospia, string diagnostico, string plan, string tratamiento,
                                    string arod, string aroi, string ardip, DateTime fechaConsulta)
        {
            try
            {
                DB.Consultas.Add(new Datos.BBDD.Consultas
                {
                    CodigoPaciente = codigo.ToUpper(),
                    MotivoConsulta = ultimaConsulta,
                    Antecedentes = antecedentes,
                    LAOD = laod,
                    LAOI= laOi,
                    LADIP= laDIP,
                    LAADD= laAdd,
                    InspeccionFisica = inspeccion,
                    Parpados = parpados,
                    Conjuntivas = conjuntivas,
                    Corneas = corneas,
                    Iris = iris,
                    Pupilas = pupilas,
                    SCOD = scOd,
                    SCOI = scOi,
                    SCJ = scJ,
                    SSOD = ssOd,
                    SSOI = ssOi,
                    SSJ = ssJ,
                    PIOI = pioi,
                    PIOD = piod,
                    Fondoscopia = fondoscopia,
                    Biomicrospia = biomicrospia,
                    Diagnostico = diagnostico,
                    Plann = plan,
                    Tratamiento = tratamiento,
                    AROD = arod,
                    AROI = aroi,
                    ARDIP = ardip,
                    FechaConsulta = fechaConsulta
                });
                DB.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                excepcion = ex.Message;
                return false;
            }
        }
    }
}
