﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Clinica.Clases;
using Clinica.Datos.BBDD;
using Clinica.Formularios;

namespace Clinica
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }
        OftalmologiaEntities DB = new OftalmologiaEntities();
        private void Login_Load(object sender, EventArgs e)
        {
            txtUser.Text = "Nombre de usuario...";
            txtUser.ForeColor = Color.LightGray;
            txtPassword.Text = "Contraseña...";
            txtPassword.ForeColor = Color.LightGray;
        }

        private void txtUser_MouseClick(object sender, MouseEventArgs e)
        {
            RemoveText(txtUser);
        }

        private void RemoveText(TextBox textBox)
        {
            if(textBox.Name == "txtUser")
            {
                if(textBox.Text == "Nombre de usuario...")
                {
                    textBox.Text = "";
                    textBox.ForeColor = Color.Black;
                }
            }
            else
            {
                if (textBox.Text == "Contraseña...")
                {
                    textBox.Text = "";
                    textBox.ForeColor = Color.Black;
                }
            }
        }
        private void AddText(TextBox textBox)
        {
            if (textBox.Name == "txtUser")
            {
                if (String.IsNullOrWhiteSpace(textBox.Text))
                {
                    textBox.Text = "Nombre de usuario...";
                    textBox.ForeColor = Color.LightGray;
                }
            }
            else
            {
                if (String.IsNullOrWhiteSpace(textBox.Text))
                {
                    textBox.Text = "Contraseña...";
                    textBox.ForeColor = Color.LightGray;
                }
            }
        }

        private void txtUser_Leave(object sender, EventArgs e)
        {
            AddText(txtUser);
        }

        private void txtPassword_MouseClick(object sender, MouseEventArgs e)
        {
            RemoveText(txtPassword);
        }

        private void txtPassword_Leave(object sender, EventArgs e)
        {
            AddText(txtPassword);
        }

        private void BtnIngresar_Click(object sender, EventArgs e)
        {
            var usuario = DB.Usuarios.SingleOrDefault(x => x.Username.ToUpper() == txtUser.Text.ToUpper() && x.Password == txtPassword.Text);

            if(usuario == null)
            {                
                if(!String.IsNullOrWhiteSpace(txtPassword.Text) && !String.IsNullOrWhiteSpace(txtUser.Text))
                {
                    MessageBox.Show("Usuario o contraseña son incorrectas, favor validar.", "Sistema Fundación Optimedica", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    MessageBox.Show("No puede dejar campos vacios. Favor verificar.", "Sistema Fundación Optimedica", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                if (!String.IsNullOrWhiteSpace(txtPassword.Text) && txtPassword.Text != "Contraseña...")
                {
                    txtPassword.ForeColor = Color.LightGray;
                    txtPassword.Text = "Contraseña...";
                }
            }
            else
            {
                if (usuario != null && usuario.Estado == true)
                {
                    PropidadesVariables.CurrentUserID = usuario.IDUsuario;
                    PropidadesVariables.CurrentUserRoleID = usuario.IdRol;
                    Principal principal = new Principal();
                    principal.Show();
                    this.Hide();
                }
                else if (usuario.Estado == false && usuario != null)
                {
                    MessageBox.Show("Su usuario se encuentra desactivado. \nFavor contactarse con el administrador.", "Sistema Fundación Optimedica", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
           
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void txtUser_TextChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrWhiteSpace(txtUser.Text))
            {
                if (txtUser.ForeColor == Color.LightGray)
                {
                    txtUser.ForeColor = Color.Black;
                }
            }
        }

        private void txtPassword_TextChanged(object sender, EventArgs e)
        {
            if(!(txtPassword.Text == "Contraseña..."))
            {
                if (txtPassword.ForeColor == Color.LightGray)
                {
                    txtPassword.ForeColor = Color.Black;
                    txtPassword.PasswordChar = '●';
                }
            }
            else
            {
                txtPassword.PasswordChar = '\0';
            }

        }

        private void txtPassword_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                BtnIngresar.PerformClick();
                e.Handled = true;
            }
        }
    }
}
