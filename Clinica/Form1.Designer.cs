﻿namespace Clinica
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BtnBuscar = new System.Windows.Forms.Button();
            this.TxtCedula = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.BtnDeletePaciente = new System.Windows.Forms.Button();
            this.BtnEditPaciente = new System.Windows.Forms.Button();
            this.BtnAddPaciente = new System.Windows.Forms.Button();
            this.TxtNombre = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.PbFotoPaciente = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.PbFotoPaciente)).BeginInit();
            this.SuspendLayout();
            // 
            // BtnBuscar
            // 
            this.BtnBuscar.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.BtnBuscar.AutoSize = true;
            this.BtnBuscar.FlatAppearance.BorderSize = 0;
            this.BtnBuscar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnBuscar.Image = global::Clinica.Properties.Resources.icons8_encuentra_hombre_usuario_321;
            this.BtnBuscar.Location = new System.Drawing.Point(844, 170);
            this.BtnBuscar.Margin = new System.Windows.Forms.Padding(4);
            this.BtnBuscar.MaximumSize = new System.Drawing.Size(124, 95);
            this.BtnBuscar.MinimumSize = new System.Drawing.Size(57, 46);
            this.BtnBuscar.Name = "BtnBuscar";
            this.BtnBuscar.Size = new System.Drawing.Size(124, 95);
            this.BtnBuscar.TabIndex = 232;
            this.BtnBuscar.UseVisualStyleBackColor = true;
            // 
            // TxtCedula
            // 
            this.TxtCedula.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.TxtCedula.Location = new System.Drawing.Point(299, 238);
            this.TxtCedula.Margin = new System.Windows.Forms.Padding(4);
            this.TxtCedula.Name = "TxtCedula";
            this.TxtCedula.Size = new System.Drawing.Size(523, 22);
            this.TxtCedula.TabIndex = 231;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(99, 242);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(133, 17);
            this.label3.TabIndex = 230;
            this.label3.Text = "Cedula del paciente";
            // 
            // BtnDeletePaciente
            // 
            this.BtnDeletePaciente.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.BtnDeletePaciente.AutoSize = true;
            this.BtnDeletePaciente.FlatAppearance.BorderSize = 0;
            this.BtnDeletePaciente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnDeletePaciente.Image = global::Clinica.Properties.Resources.icons8_retire_hombre_usuario_32;
            this.BtnDeletePaciente.Location = new System.Drawing.Point(592, 290);
            this.BtnDeletePaciente.Margin = new System.Windows.Forms.Padding(4);
            this.BtnDeletePaciente.MaximumSize = new System.Drawing.Size(124, 95);
            this.BtnDeletePaciente.MinimumSize = new System.Drawing.Size(57, 46);
            this.BtnDeletePaciente.Name = "BtnDeletePaciente";
            this.BtnDeletePaciente.Size = new System.Drawing.Size(124, 95);
            this.BtnDeletePaciente.TabIndex = 229;
            this.BtnDeletePaciente.UseVisualStyleBackColor = true;
            // 
            // BtnEditPaciente
            // 
            this.BtnEditPaciente.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.BtnEditPaciente.AutoSize = true;
            this.BtnEditPaciente.FlatAppearance.BorderSize = 0;
            this.BtnEditPaciente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnEditPaciente.Image = global::Clinica.Properties.Resources.icons8_editar_usuario_masculino_32;
            this.BtnEditPaciente.Location = new System.Drawing.Point(460, 290);
            this.BtnEditPaciente.Margin = new System.Windows.Forms.Padding(4);
            this.BtnEditPaciente.MaximumSize = new System.Drawing.Size(124, 95);
            this.BtnEditPaciente.MinimumSize = new System.Drawing.Size(57, 46);
            this.BtnEditPaciente.Name = "BtnEditPaciente";
            this.BtnEditPaciente.Size = new System.Drawing.Size(124, 95);
            this.BtnEditPaciente.TabIndex = 228;
            this.BtnEditPaciente.UseVisualStyleBackColor = true;
            // 
            // BtnAddPaciente
            // 
            this.BtnAddPaciente.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.BtnAddPaciente.AutoSize = true;
            this.BtnAddPaciente.FlatAppearance.BorderSize = 0;
            this.BtnAddPaciente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnAddPaciente.Image = global::Clinica.Properties.Resources.icons8_añadir_usuario_masculino_32;
            this.BtnAddPaciente.Location = new System.Drawing.Point(328, 290);
            this.BtnAddPaciente.Margin = new System.Windows.Forms.Padding(4);
            this.BtnAddPaciente.MaximumSize = new System.Drawing.Size(124, 95);
            this.BtnAddPaciente.MinimumSize = new System.Drawing.Size(57, 46);
            this.BtnAddPaciente.Name = "BtnAddPaciente";
            this.BtnAddPaciente.Size = new System.Drawing.Size(124, 95);
            this.BtnAddPaciente.TabIndex = 227;
            this.BtnAddPaciente.UseVisualStyleBackColor = true;
            // 
            // TxtNombre
            // 
            this.TxtNombre.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.TxtNombre.Location = new System.Drawing.Point(299, 206);
            this.TxtNombre.Margin = new System.Windows.Forms.Padding(4);
            this.TxtNombre.Name = "TxtNombre";
            this.TxtNombre.Size = new System.Drawing.Size(523, 22);
            this.TxtNombre.TabIndex = 226;
            // 
            // textBox1
            // 
            this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.textBox1.Location = new System.Drawing.Point(299, 174);
            this.textBox1.Margin = new System.Windows.Forms.Padding(4);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(523, 22);
            this.textBox1.TabIndex = 225;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(99, 210);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(140, 17);
            this.label2.TabIndex = 224;
            this.label2.Text = "Nombre del Paciente";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(99, 181);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(130, 17);
            this.label1.TabIndex = 223;
            this.label1.Text = "Codigo de paciente";
            // 
            // PbFotoPaciente
            // 
            this.PbFotoPaciente.Location = new System.Drawing.Point(146, 374);
            this.PbFotoPaciente.Margin = new System.Windows.Forms.Padding(4);
            this.PbFotoPaciente.Name = "PbFotoPaciente";
            this.PbFotoPaciente.Size = new System.Drawing.Size(125, 110);
            this.PbFotoPaciente.TabIndex = 233;
            this.PbFotoPaciente.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1067, 554);
            this.Controls.Add(this.PbFotoPaciente);
            this.Controls.Add(this.BtnBuscar);
            this.Controls.Add(this.TxtCedula);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.BtnDeletePaciente);
            this.Controls.Add(this.BtnEditPaciente);
            this.Controls.Add(this.BtnAddPaciente);
            this.Controls.Add(this.TxtNombre);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.PbFotoPaciente)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BtnBuscar;
        private System.Windows.Forms.TextBox TxtCedula;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button BtnDeletePaciente;
        private System.Windows.Forms.Button BtnEditPaciente;
        private System.Windows.Forms.Button BtnAddPaciente;
        private System.Windows.Forms.TextBox TxtNombre;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox PbFotoPaciente;
    }
}