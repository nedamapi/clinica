CREATE DATABASE Oftalmologia

USE Oftalmologia

CREATE TABLE Pacientes
(
	ID INT IDENTITY PRIMARY KEY,
	CodigoPaciente VARCHAR(30) CONSTRAINT I_CodigoPaciente UNIQUE,
	Nombre VARCHAR(50),
	Cedula VARCHAR(11),
	FechaNacimiento DATE,
	Edad INT,
	IdSexo INT,
	Telefono VARCHAR(12),
	Oficio VARCHAR(100),
	FechaCreacion DATE,
	IdUsuarioCreador INT, 
	IdUsuarioModifica INT,
	FechaModificacion DATE,
	Estado BIT
)

CREATE TABLE Sexos
(
	IdSexo INT IDENTITY PRIMARY KEY,
	Descripcion VARCHAR(10)
)

CREATE TABLE Consultas 
(
	ID INT IDENTITY PRIMARY KEY,
	CodigoPaciente VARCHAR(30),
	MotivoConsulta VARCHAR(MAX),
	Antecedentes VARCHAR(MAX),
	LAOD VARCHAR(15),
	LAOI VARCHAR(15),
	LADIP VARCHAR(15),
	LAADD VARCHAR(15),
	InspeccionFisica VARCHAR(256),
	Parpados VARCHAR(256),
	Conjuntivas VARCHAR(256),
	Corneas VARCHAR(256),
	Iris VARCHAR(256),
	Pupilas VARCHAR(256),
	SCOD VARCHAR(15),
	SCOI VARCHAR(15),
	SCJ VARCHAR(15),
	SSOD VARCHAR(15),
	SSOI VARCHAR(15),
	SSJ VARCHAR(15),
	PIOD VARCHAR(15),
	PIOI VARCHAR(15),
	AROD VARCHAR(15),
	AROI VARCHAR(15),
	ARDIP VARCHAR(15),
	Fondoscopia VARCHAR(MAX),
	Biomicrospia VARCHAR(MAX),
	Diagnostico VARCHAR(MAX),
	Plann VARCHAR(MAX),
	Tratamiento VARCHAR(MAX),
	FechaConsulta DATE
)

CREATE TABLE Usuarios
(
	IDUsuario INT IDENTITY PRIMARY KEY,
	Nombres VARCHAR(256) NOT NULL,
	Apellidos VARCHAR(256),
	Username VARCHAR(50) NOT NULL,
	Password VARCHAR(100) NOT NULL,
	IdRol INT,
	Estado BIT,
	FechaCreacion DATE,
	FechaModificacion DATE
)

CREATE TABLE Roles
(
	IdRol INT IDENTITY PRIMARY KEY,
	Descripcion VARCHAR(60)
)

CREATE TABLE FotoPaciente
(	
	ID INT IDENTITY PRIMARY KEY,
	CodigoPaciente VARCHAR(30),
	RutaImagen VARCHAR(MAX)
)

ALTER TABLE Usuarios ADD CONSTRAINT UserRol FOREIGN KEY (IdRol)
REFERENCES Roles(IdRol)

ALTER TABLE Consultas ADD CONSTRAINT CodigoPacienteConsulta FOREIGN KEY (CodigoPaciente)
REFERENCES Pacientes(CodigoPaciente)

ALTER TABLE Pacientes ADD CONSTRAINT SexoPaciente FOREIGN KEY (IdSexo)
REFERENCES Sexos(IdSexo)

ALTER TABLE FotoPaciente ADD CONSTRAINT FK_FotoPaciente FOREIGN KEY (CodigoPaciente)
REFERENCES Pacientes(CodigoPaciente)